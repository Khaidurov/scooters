<?php

/* @var $this yii\web\View */
/* @var $model common\modules\article\models\Article */
?>
<div class="row">
    <div class="col-md-12">
        <h3><?= $model->title ?></h3>
        <?= $model->desc ?>
    </div>
</div>