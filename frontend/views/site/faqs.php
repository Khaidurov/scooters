<?php

use yii\bootstrap\Collapse;

/* @var $this yii\web\View */
/* @var $models common\modules\faq\models\Faq */
?>
<div class="row">
    <div class="col-md-12">
        <?php
            $items = [];
            foreach ($models as $model) {
                if ($items) {
                    array_push($items, array(
                        'label' => $model->question,
                        'content' => $model->answer,
                    ));
                } else {
                    array_push($items, array(
                        'label' => $model->question,
                        'content' => $model->answer,
                        'contentOptions' => ['class' => 'in']
                    ));
                }

            }

            echo Collapse::widget([
                'items' => $items,
            ])
        ?>
    </div>
</div>