<?php
namespace frontend\controllers;

use common\modules\article\models\Article;
use common\modules\faq\models\Faq;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\form\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Displays article.
     *
     * @return mixed
     */
    public function actionArticle($id)
    {
        $this->layout = 'clear';

        $model = Article::findOne($id);

        return $this->render('article', [
            'model' => $model,
        ]);
    }

    /**
     * Displays FAQs.
     *
     * @return mixed
     */
    public function actionFaqs()
    {
        $this->layout = 'clear';

        $models = Faq::find()->active()->all();

        return $this->render('faqs', [
            'models' => $models,
        ]);
    }

    /**
     * Display payment success page.
     *
     * @return mixed
     */
    public function actionPaymentSuccess()
    {
        $this->layout = 'clear';

        return $this->render('payment-success');
    }

    /**
     * Display payment failure page.
     *
     * @return mixed
     */
    public function actionPaymentFailure()
    {
        $this->layout = 'clear';

        return $this->render('payment-success');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
