<?php

namespace console\controllers;

use common\modules\user\models\User;
use Yii;
use yii\console\Controller;
/**
 * Инициализатор RBAC выполняется в консоли php yii rbac/init
 */
class RbacController extends Controller {

    public function actionInit() {
        $auth = Yii::$app->authManager;

        $auth->removeAll();

        $admin = $auth->createRole('admin');
        $moderator = $auth->createRole('moderator');
        $issue_point_admin = $auth->createRole('issue_point_admin');
        $user = $auth->createRole('user');

        $auth->add($admin);
        $auth->add($moderator);
        $auth->add($issue_point_admin);
        $auth->add($user);

        $auth->addChild($admin, $moderator);
        $auth->addChild($admin, $issue_point_admin);
        $auth->addChild($admin, $user);

    }

    public function actionIssuePointAdmin() {
        $model = new User();

        $model->login = 'issue_point_admin';
        $model->setPassword('issue_point_admin');

        if ($model->validate() && $model->save()) {

            $auth = Yii::$app->authManager;
            $editor = $auth->getRole('issue_point_admin');
            $auth->assign($editor, $model->id);

            return 'Success';

        } else {
            return 'Error';
        }

    }
}

