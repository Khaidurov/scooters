<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tariff`.
 */
class m180816_204440_create_tariff_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tariff', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),

            'name' => $this->string(),
            'desc' => $this->text(),
            'time' => $this->integer(),
            'price' => $this->double(),
            'device_type_id' => $this->integer(),


            'status' => $this->tinyInteger()->notNull()->defaultValue(1),
        ]);

        $this->addForeignKey(
            'tariff-device_type',
            'tariff',
            'device_type_id',
            'device_type',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'tariff-user-created_by',
            'tariff',
            'created_by',
            'user',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'tariff-user-updated_by',
            'tariff',
            'updated_by',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tariff');

        $this->dropForeignKey(
            'tariff-device_type',
            'device_type'
        );

        $this->dropForeignKey(
            'tariff-user-created_by',
            'user'
        );

        $this->dropForeignKey(
            'tariff-user-created_by',
            'user'
        );
    }
}
