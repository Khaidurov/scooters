<?php

use yii\db\Migration;

/**
 * Handles the creation of table `device`.
 */
class m180816_205640_create_device_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('device', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),

            'name' => $this->string(),
            'model' => $this->string(),
            'teh_status' => $this->tinyInteger()->notNull()->defaultValue(1),
            'issue_point_id' => $this->integer(),
            'uses_count' => $this->integer(),
            'device_type_id' => $this->integer(),

            'status' => $this->tinyInteger()->notNull()->defaultValue(1),
        ]);

        $this->addForeignKey(
            'device-issue_point',
            'device',
            'issue_point_id',
            'issue_point',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'device-device_type',
            'device',
            'device_type_id',
            'device_type',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'device-user-created_by',
            'device',
            'created_by',
            'user',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'device-user-updated_by',
            'device',
            'updated_by',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('device');

        $this->dropForeignKey(
            'device-issue_point',
            'issue_point'
        );

        $this->dropForeignKey(
            'device-device_type',
            'device_type'
        );

        $this->dropForeignKey(
            'device-user-created_by',
            'user'
        );

        $this->dropForeignKey(
            'device-user-created_by',
            'user'
        );
    }
}
