<?php

use yii\db\Migration;

/**
 * Handles the creation of table `device_type`.
 */
class m180816_203806_create_device_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('device_type', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),

            'name' => $this->string(),
            'desc' => $this->text(),
            'img' => $this->string(),

            'status' => $this->tinyInteger()->notNull()->defaultValue(1),
        ]);

        $this->addForeignKey(
            'device_type-user-created_by',
            'device_type',
            'created_by',
            'user',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'device_type-user-updated_by',
            'device_type',
            'updated_by',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('device_type');

        $this->dropForeignKey(
            'device_type-user-created_by',
            'user'
        );

        $this->dropForeignKey(
            'device_type-user-created_by',
            'user'
        );
    }
}
