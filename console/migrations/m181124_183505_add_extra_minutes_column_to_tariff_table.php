<?php

use yii\db\Migration;

/**
 * Handles adding extra_minutes to table `tariff`.
 */
class m181124_183505_add_extra_minutes_column_to_tariff_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tariff', 'extra_minutes', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tariff', 'extra_minutes');
    }
}
