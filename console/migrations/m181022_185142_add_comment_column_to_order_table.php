<?php

use yii\db\Migration;

/**
 * Handles adding comment to table `order`.
 */
class m181022_185142_add_comment_column_to_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order', 'comment', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order', 'comment');
    }
}
