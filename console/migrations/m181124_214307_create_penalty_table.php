<?php

use yii\db\Migration;

/**
 * Handles the creation of table `penalty`.
 */
class m181124_214307_create_penalty_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('penalty', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),

            'name' => $this->string(),
            'desc' => $this->text(),
            'price' => $this->double(),

            'status' => $this->tinyInteger()->notNull()->defaultValue(1),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('penalty');
    }
}
