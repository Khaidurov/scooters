<?php

use yii\db\Migration;

/**
 * Handles the creation of table `issue_point`.
 */
class m180816_205018_create_issue_point_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('issue_point', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),

            'name' => $this->string(),
            'address' => $this->string(),
            'lat' => $this->double(),
            'lng' => $this->double(),
            'desc_full' => $this->text(),
            'desc_short' => $this->string(),
            'admin_id' => $this->integer(),

            'status' => $this->tinyInteger()->notNull()->defaultValue(1),
        ]);

        $this->addForeignKey(
            'issue_point-user',
            'issue_point',
            'admin_id',
            'user',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'issue_point-user-created_by',
            'issue_point',
            'created_by',
            'user',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'issue_point-user-updated_by',
            'issue_point',
            'updated_by',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('issue_point');

        $this->dropForeignKey(
            'issue_point-user',
            'user'
        );

        $this->dropForeignKey(
            'issue_point-user-created_by',
            'user'
        );

        $this->dropForeignKey(
            'issue_point-user-created_by',
            'user'
        );
    }
}
