<?php

use yii\db\Migration;

/**
 * Handles adding penalty_id to table `order`.
 */
class m181124_220122_add_penalty_id_column_to_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order', 'penalty_id', $this->integer());

        $this->addForeignKey(
            'order-penalty',
            'order',
            'penalty_id',
            'penalty',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order', 'penalty_id');
    }
}
