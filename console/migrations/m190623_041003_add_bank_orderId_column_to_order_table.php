<?php

use yii\db\Migration;

/**
 * Handles adding bank_orderId to table `order`.
 */
class m190623_041003_add_bank_orderId_column_to_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order', 'bank_orderId', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order', 'bank_orderId');
    }
}
