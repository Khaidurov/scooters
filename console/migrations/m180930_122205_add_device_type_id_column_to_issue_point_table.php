<?php

use yii\db\Migration;

/**
 * Handles adding device_type_id to table `issue_point`.
 */
class m180930_122205_add_device_type_id_column_to_issue_point_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('issue_point', 'device_type_id', $this->integer());

        $this->addForeignKey(
            'issue_point-device_type',
            'issue_point',
            'device_type_id',
            'device_type',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('issue_point', 'device_type_id');

        $this->dropForeignKey(
            'issue_point-device_type',
            'device_type'
        );
    }
}
