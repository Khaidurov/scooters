<?php

use yii\db\Migration;

/**
 * Handles adding bank_billingId to table `user_profile`.
 */
class m190623_040431_add_bank_billingId_column_to_user_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_profile', 'bank_billingId', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user_profile', 'bank_billingId');
    }
}
