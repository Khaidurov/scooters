<?php

use yii\db\Migration;

/**
 * Handles adding device_id to table `order`.
 */
class m181001_215342_add_device_id_column_to_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order', 'device_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order', 'device_id');
    }
}
