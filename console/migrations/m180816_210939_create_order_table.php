<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order`.
 */
class m180816_210939_create_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),

            'date_start' => $this->integer(),
            'date_end' => $this->integer(),
            'time' => $this->integer(),
            'amount_price' => $this->double(),
            'admin_start_id' => $this->integer(),
            'admin_end_id' => $this->integer(),
            'client_id' => $this->integer(),
            'tariff_id' => $this->integer(),
            'issue_point_start_id' => $this->integer(),
            'issue_point_end_id' => $this->integer(),
            'live_status' => $this->tinyInteger()->notNull()->defaultValue(0),
            'scooter_state' => $this->tinyInteger()->notNull()->defaultValue(1),
            'signature_img' => $this->string(),

            'status' => $this->tinyInteger()->notNull()->defaultValue(1),
        ]);

        $this->addForeignKey(
            'order-user-admin_start_id',
            'order',
            'admin_start_id',
            'user',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'order-user-admin_end_id',
            'order',
            'admin_end_id',
            'user',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'order-user-client_id',
            'order',
            'client_id',
            'user',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'order-tariff',
            'order',
            'tariff_id',
            'tariff',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'order-issue_point-issue_point_start_id',
            'order',
            'issue_point_start_id',
            'issue_point',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'order-issue_point-issue_point_end_id',
            'order',
            'issue_point_end_id',
            'issue_point',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'order-user-created_by',
            'order',
            'created_by',
            'user',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'order-user-updated_by',
            'order',
            'updated_by',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('order');

        $this->dropForeignKey(
            'order-user-admin_start_id',
            'user'
        );

        $this->dropForeignKey(
            'order-user-admin_end_id',
            'user'
        );

        $this->dropForeignKey(
            'order-user-client_id',
            'user'
        );

        $this->dropForeignKey(
            'order-tariff',
            'tariff'
        );

        $this->dropForeignKey(
            'order-issue_point-issue_point_start_id',
            'issue_point'
        );

        $this->dropForeignKey(
            'order-issue_point-issue_point_end_id',
            'issue_point'
        );

        $this->dropForeignKey(
            'order-user-created_by',
            'user'
        );

        $this->dropForeignKey(
            'order-user-created_by',
            'user'
        );
    }
}
