<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\faq\models\search\FaqSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Faqs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faq-index">


    <?php Pjax::begin(); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <p>
                        <?= Html::a('Создать Faq', ['create'], ['class' => 'btn btn-success']) ?>
                    </p>
                </div>
                <div class="panel-body">
                                            <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
        'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                                    'id',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
            //'question',
            //'answer:ntext',
            //'status',

                        ['class' => 'yii\grid\ActionColumn'],
                        ],
                        'options' => ['class' => 'table-responsive'],
                        ]); ?>
                                    </div>
            </div>
        </div>
    </div>
    <?php Pjax::end(); ?>
</div>
