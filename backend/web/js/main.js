$(document).ready(function() { // Ждём загрузки страницы

    $(".image-popup").click(function(){	// Событие клика на маленькое изображение
        var img = $(this);	// Получаем изображение, на которое кликнули
        var src = img.attr('src'); // Достаем из этого изображения путь до картинки
        $("body").append("<div class='popup_container'>"+ //Добавляем в тело документа разметку всплывающего окна
            "<div class='popup_bg'></div>"+ // Блок, который будет служить фоном затемненным
            "<img src='"+src+"' class='popup_img' />"+ // Само увеличенное фото
            "</div>");
        $(".popup_container").fadeIn(200); // Медленно выводим изображение
        $(".popup_bg").click(function(){	// Событие клика на затемненный фон
            $(".popup_container").fadeOut(200);	// Медленно убираем всплывающее окн
            setTimeout(function() {	// Выставляем таймер
                $(".popup_container").remove(); // Удаляем разметку всплывающего окна
            }, 200);
        });
    });

});
