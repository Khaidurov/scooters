<?php
namespace  backend\models\form;

use Yii;
use common\models\Device;
use common\models\IssuePoint;
use common\models\Order;
use common\models\Penalty;
use common\models\Tariff;
use yii\base\Model;

/**
 * OrderApprove form
 */
class ApproveOrderForm extends Model
{
    public $id;
    public $scooter_state;
    public $penalty_id;
    public $comment;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'trim'],
            [['id'], 'required'],
            [['id', 'scooter_state', 'penalty_id'], 'integer'],
            [['comment'], 'string'],
            [['id'], 'exist', 'targetClass' => Order::className(), 'targetAttribute' => ['id' => 'id']],
            [['penalty_id'], 'exist', 'targetClass' => Penalty::className(), 'targetAttribute' => ['penalty_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID заказа',
            'scooter_state' => 'Состояние устройства',
            'comment' => 'Комментарий к заказу',
            'penalty_id' => 'Штраф',
        ];
    }

    public function approve()
    {
        $issuePointModel =  IssuePoint::getIssuePointDetailByAdmin(Yii::$app->user->id);

        $model = Order::findOne($this->id);
        $model->issue_point_end_id = $issuePointModel->id;
        $model->admin_end_id = $issuePointModel->admin_id;
        $model->live_status = Order::LIVE_STATUS_APPROVED;
        $model->scooter_state = $this->scooter_state;
        $model->penalty_id = $this->penalty_id;
        $model->comment = $this->comment;
        $model->date_end = time();
        $model->amount_price = $this->getPrice($model->date_start, $model->date_end, $model->tariff_id);


        Device::afterEndOrder($model->device_id, $model->issue_point_end_id);

        if ($model->validate() && $model->save()) {

            return $model->id;

        } else {

            return array(
                'error' => "Ошибка подтверждения заказа"
            );

        }
    }

    public function getPrice ($date_start, $date_end, $tariff_id) {

        $tariff = Tariff::findOne($tariff_id);

        $time = ($date_end - $date_start) / 60;

        $price = intdiv($time, $tariff->time) * $tariff->price;

        if (($time % $tariff->time) > $tariff->extra_minutes) {
            $price += $tariff->price;
        }

        return $price;
    }

}