<?php
namespace  backend\models\form;

use common\models\Order;
use yii\base\Model;

/**
 * OrderConfirm form
 */
class ConfirmOrderForm extends Model
{
    public $id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'trim'],
            [['id'], 'required'],
            [['id'], 'integer'],
            [['id'], 'exist', 'targetClass' => Order::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID заказа',
        ];
    }

    public function confirm()
    {
        $orderModel = Order::findOne($this->id);

        $orderModel->date_start = time();
        $orderModel->live_status = Order::LIVE_STATUS_LEASE;

        $orderModel->save();

        return $this->id;
    }

}