<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'language' => 'ru_RU',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'on beforeRequest' => function ($event) {
        if(!Yii::$app->request->isSecureConnection){
            $url = Yii::$app->request->getAbsoluteUrl();
            $url = str_replace('http:', 'https:', $url);
            Yii::$app->getResponse()->redirect($url);
            Yii::$app->end();
        }
    },
    'modules' => [
        'user' => [
            'class' => 'common\modules\user\User',
        ],
        'userprofile' => [
            'class' => 'common\modules\userprofile\UserProfile',
        ],
        'article' => [
            'class' => 'common\modules\article\Article',
        ],
        'faq' => [
            'class' => 'common\modules\faq\Faq',
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            'baseUrl' => '/admin',
        ],
        'user' => [
            'loginUrl' => ['user/default/login'],
            'identityClass' => 'common\modules\user\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/index',
                '<controller:\w+>/<action:\w+>/' => '<controller>/<action>',
            ],
        ],
        'formatter' => [
            'locale'=>'ru_RU',
            'dateFormat' => 'php:d.n.Y',
            'datetimeFormat' => 'php:d.n.Y H:i',
        ],

    ],
    'as access' => [
        'class' => 'yii\filters\AccessControl',
        'except' => ['user/default/login'],
        'rules' => [
            [
                'allow' => true,
                'roles' => ['@'],
            ]
        ]
    ],
    'params' => $params,
];
