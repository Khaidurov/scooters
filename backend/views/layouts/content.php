<?php
/* @var $content string */

use common\widgets\Alert;

?>

<div class="page">
    <header class="ribbon">
        <h2>
            <?=  $this->title; ?>
        </h2>
    </header>
    <div class="page-content">
        <div class="container-fluid">
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>
</div>