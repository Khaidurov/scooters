<?php

use common\widgets\IconMenu;

echo IconMenu::widget([
    'options' => ['class' => 'main-nav', 'id' => 'main-nav'],
    'items' => [
        ['label' => 'Меню', 'options' => ['class' => 'main-nav-label']],
        ['label' => 'Рабочий стол', 'icon' => 'monitor', 'url' => ['/'], 'visible' => Yii::$app->user->can('admin')],
        [
            'label' => 'Записи',
            'icon' => 'book-open',
            'url' => [''],
            'items' => [
                ['label' => 'Тип записей', 'url' => ['/article/articles-type-crud/']],
                ['label' => 'Записи', 'url' => ['/article/article-crud/']],
            ],
            'visible' => Yii::$app->user->can('admin')
        ],
        ['label' => 'FAQ', 'icon' => 'forum', 'url' => ['/faq/crud'], 'visible' => Yii::$app->user->can('admin')],
        ['label' => 'Точки выдачи', 'icon' => 'map-marker', 'url' => ['/issue-point'], 'visible' => Yii::$app->user->can('admin')],
        [
            'label' => 'Устройства',
            'icon' => 'bike',
            'url' => [''],
            'items' => [
                ['label' => 'Тип устройства', 'url' => ['/device-type']],
                ['label' => 'Устройства', 'url' => ['/device']],
            ],
            'visible' => Yii::$app->user->can('admin')
        ],
        ['label' => 'Тарифы', 'icon' => 'currency-usd', 'url' => ['/tariff'], 'visible' => Yii::$app->user->can('admin')],
        ['label' => 'Заказы', 'icon' => 'view-list', 'url' => ['/order'], 'visible' => Yii::$app->user->can('admin')],
        ['label' => 'Профили пользователей', 'icon' => 'account', 'url' => ['/userprofile/user-profile-crud'], 'visible' => Yii::$app->user->can('admin')],
    ]
]);