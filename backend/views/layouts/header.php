<nav class="navbar navbar-blue navbar-fixed-top topbar" style="" id="topbar">

    <div class="navbar-header navbar-always-float navbar-left">
        <button class="btn btn-transparent-light navbar-btn navbar-sidebar-toggle  hidden-xs ">
            <i class="mdi mdi-menu"></i>
        </button>
        <button class="btn btn-transparent-light navbar-btn navbar-sidebar-collapse hidden-sm hidden-md hidden-lg">
            <i class="mdi mdi-menu"></i>
        </button>
        <a class="navbar-brand" href="<?= Yii::$app->homeUrl ?>">
            <i class="mdi mdi-cellphone-iphone"></i><span class="navbar-brand-text">Scooters</span>
        </a>
    </div>

</nav>