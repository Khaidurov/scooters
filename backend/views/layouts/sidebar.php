<?php

use common\widgets\IconMenu;
use yii\helpers\Html;
?>
<div class="sidebar sidebar-left sidebar-dark sidebar-fixed sidebar-navbar-theme" id="sidebar">

    <div class="sidebar-scrollable-content">

        <div class="sidebar-body">

            <div class="sidebar-cover">
                <div class="sidebar-user">
                    <div class="sidebar-user-img">
                        <img src="/backend/web/img/user-avatar.png" alt="" class="img-circle img-online img-thumbnailimg-thumbnail-primary">
                    </div>
                    <div class="sidebar-user-name">
                        <?= Yii::$app->user->identity->userProfile->name . ' ' . Yii::$app->user->identity->userProfile->surname?>
                        <span class="text-small sidebar-user-email"><?= Yii::$app->user->identity->login?></span>
                    </div>
                </div>
            </div>

            <div class="main-menu-container">

                <?php if (Yii::$app->user->can('admin')) {
                    echo $this->render('admin-menu');
                } elseif (Yii::$app->user->can('issue_point_admin')) {
                    echo $this->render('issue-point-admin-menu');
                }?>



            </div>

        </div>

    </div>

    <div class="sidebar-footer">
        <div class="horizontal-nav">



            <ul class="horizontal-nav horizontal-nav-3">
                <li>
                    <a href=""><i class="mdi mdi-settings"></i></a>
                </li>
                <li>
                    <a href=""><i class="mdi mdi-account"></i></a>
                </li>
                <li>
                    <?php echo Html::beginForm(['/user/default/logout'], 'post')
                    . Html::submitButton(
                        '<i class="mdi mdi-logout"></i>',
                        ['class' => '']
                    )
                    . Html::endForm() ?>
                </li>
            </ul>
        </div>
    </div>

</div>