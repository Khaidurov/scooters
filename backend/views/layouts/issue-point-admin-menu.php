<?php

use common\widgets\IconMenu;

echo IconMenu::widget([
    'options' => ['class' => 'main-nav', 'id' => 'main-nav'],
    'items' => [
        ['label' => 'Меню', 'options' => ['class' => 'main-nav-label']],
        ['label' => 'Точка выдачи', 'icon' => 'map-marker', 'url' => ['/issue-point-admin']],
        ['label' => 'Устройства', 'icon' => 'bike', 'url' => ['/issue-point-admin/devices']],
        ['label' => 'Заказы', 'icon' => 'view-list', 'url' => ['/issue-point-admin/orders']],
        ['label' => 'Новый заказ', 'icon' => 'arrow-up', 'url' => ['/issue-point-admin/confirm-order']],
        ['label' => 'Закончить заказ', 'icon' => 'arrow-down', 'url' => ['/issue-point-admin/approve-order']],
    ]
]);