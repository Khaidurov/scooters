<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\DeviceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Устройства';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="device-index">


    <?php Pjax::begin(); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <p>
                        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
                    </p>
                </div>
                <div class="panel-body">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            'id',
                            'name',
                            'model',
                            [
                                'attribute' => 'teh_status',
                                'filter' => Select2::widget([
                                    'model'=>$searchModel,
                                    'attribute'=>'teh_status',
                                    'data' => common\models\Device::getTehStatusList(),
                                    'options' => ['placeholder' => 'Выбрать'],

                                ]),
                                'value' => function($model) {
                                    return $model::getTehStatusList()[$model->teh_status];
                                },
                            ],
                            [
                                'attribute' => 'issue_point_id',
                                'filter' => Select2::widget([
                                    'model'=>$searchModel,
                                    'attribute'=>'issue_point_id',
                                    'data' => common\models\Device::getIssuePointList(),
                                    'options' => ['placeholder' => 'Выбрать'],

                                ]),
                                'value' => function($model) {
                                    return $model->issuePoint->name;
                                },
                            ],
                            [
                                'attribute' => 'device_type_id',
                                'filter' => Select2::widget([
                                    'model'=>$searchModel,
                                    'attribute'=>'device_type_id',
                                    'data' => common\models\Device::getDeviceTypeList(),
                                    'options' => ['placeholder' => 'Выбрать'],

                                ]),
                                'value' => function($model) {
                                    return $model->deviceType->name;
                                },

                            ],
                            'uses_count',
                            [
                                'attribute' => 'status',
                                'filter' => Select2::widget([
                                    'model'=>$searchModel,
                                    'attribute'=>'status',
                                    'data' => common\models\Device::getSatatusList(),
                                    'options' => ['placeholder' => 'Выбрать'],

                                ]),
                                'value' => function($model) {
                                    return $model::getSatatusList()[$model->status];
                                },
                            ],

                        ['class' => 'yii\grid\ActionColumn'],
                        ],
                        'options' => ['class' => 'table-responsive'],
                        ]); ?>
                </div>
            </div>
        </div>
    </div>
    <?php Pjax::end(); ?>
</div>
