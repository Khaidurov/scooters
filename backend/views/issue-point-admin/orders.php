<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">


    <?php Pjax::begin(); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <p>
                        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
                    </p>
                </div>
                <div class="panel-body">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            'id',
                            //'date_start',
                            //'date_end',
                            [
                                'attribute' => 'client',
                                'value' => function($model) {
                                    return $model->client->phone_number;
                                },
                            ],
                            [
                                'attribute' => 'admin_start',
                                'value' => function($model) {
                                    return $model->adminStart->login;
                                },
                            ],
                            [
                                'attribute' => 'admin_end',
                                'value' => function($model) {
                                    return $model->adminEnd->login;
                                },
                            ],
                            [
                                'attribute' => 'issue_point_start_id',
                                'filter' => Select2::widget([
                                    'model'=>$searchModel,
                                    'attribute'=>'issue_point_start_id',
                                    'data' => common\models\Order::getIssuePointList(),
                                    'options' => ['placeholder' => 'Выбрать'],

                                ]),
                                'value' => function($model) {
                                    return $model->issuePointStart->name;
                                },
                            ],
                            [
                                'attribute' => 'issue_point_end_id',
                                'filter' => Select2::widget([
                                    'model'=>$searchModel,
                                    'attribute'=>'issue_point_end_id',
                                    'data' => common\models\Order::getIssuePointList(),
                                    'options' => ['placeholder' => 'Выбрать'],

                                ]),
                                'value' => function($model) {
                                    return $model->issuePointEnd->name;
                                },
                            ],
                            [
                                'attribute' => 'tariff_id',
                                'filter' => Select2::widget([
                                    'model'=>$searchModel,
                                    'attribute'=>'tariff_id',
                                    'data' => common\models\Order::getTariffList(),
                                    'options' => ['placeholder' => 'Выбрать'],

                                ]),
                                'value' => function($model) {
                                    return $model->tariff->name;
                                },
                            ],
                            'amount_price',
                            [
                                'attribute' => 'scooter_state',
                                'filter' => Select2::widget([
                                    'model'=>$searchModel,
                                    'attribute'=>'scooter_state',
                                    'data' => common\models\Order::getScooterStateList(),
                                    'options' => ['placeholder' => 'Выбрать'],

                                ]),
                                'value' => function($model) {
                                    return $model->getScooterStateList()[$model->scooter_state];
                                },

                            ],
                            [
                                'attribute' => 'live_status',
                                'filter' => Select2::widget([
                                    'model'=>$searchModel,
                                    'attribute'=>'live_status',
                                    'data' => common\models\Order::getLiveStatusList(),
                                    'options' => ['placeholder' => 'Выбрать'],

                                ]),
                                'value' => function($model) {
                                    return $model->getLiveStatusList()[$model->live_status];
                                },
                            ],
                            [
                                'class' => \yii\grid\ActionColumn::className(),
                                'buttons'=>[
                                    'view'=>function ($url, $model) {
                                        $customurl=Yii::$app->getUrlManager()->createUrl(['issue-point-admin/order-view','id'=>$model['id']]); //$model->id для AR
                                        return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', $customurl,
                                            ['title' => Yii::t('yii', 'View'), 'data-pjax' => '0']);
                                    }
                                ],
                                'template'=>'{view}',
                            ],
                        ],
                        'options' => ['class' => 'table-responsive'],
                        ]); ?>
                </div>
            </div>
        </div>
    </div>
    <?php Pjax::end(); ?>
</div>
