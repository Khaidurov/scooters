<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Order */

$this->title = 'Заказ: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="table-responsive">
                        <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            [
                                'attribute' => 'date_start',
                                'value' => function($model) {
                                    return Yii::$app->formatter->format($model->date_start, 'datetime' );
                                },
                            ],
                            [
                                'attribute' => 'date_end',
                                'value' => function($model) {
                                    return Yii::$app->formatter->format($model->date_end, 'datetime' );
                                },
                            ],
                            [
                                'attribute' => 'client',
                                'value' => function($model) {
                                    return $model->client->phone_number;
                                },
                            ],
                            [
                                'attribute' => 'admin_start',
                                'value' => function($model) {
                                    return $model->adminStart->login;
                                },
                            ],
                            [
                                'attribute' => 'admin_end',
                                'value' => function($model) {
                                    return $model->adminEnd->login;
                                },
                            ],
                            [
                                'attribute' => 'issue_point_start_id',
                                'value' => function($model) {
                                    return $model->issuePointStart->name;
                                },
                            ],
                            [
                                'attribute' => 'issue_point_end_id',
                                'value' => function($model) {
                                    return $model->issuePointEnd->name;
                                },
                            ],
                            [
                                'attribute' => 'tariff_id',
                                'value' => function($model) {
                                    return $model->tariff->name;
                                },
                            ],
                            'amount_price',
                            [
                                'attribute' => 'scooter_state',
                                'value' => function($model) {
                                    return $model->getScooterStateList()[$model->scooter_state];
                                },

                            ],
                            [
                                'attribute' => 'live_status',
                                'value' => function($model) {
                                    return $model->getLiveStatusList()[$model->live_status];
                                },
                            ],
                            'comment',
                            [
                                'attribute' => 'penalty_id',
                                'value' => function($model) {
                                    return $model->penalty->name;
                                },
                            ],
                        ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
