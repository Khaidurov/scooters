<?php

use yii\widgets\ActiveForm;
use backend\assets\QrReaderAsset;
use backend\assets\ApproveOrderAsset;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $modelForm backend\models\form\ApproveOrderForm */

QrReaderAsset::register($this);
ApproveOrderAsset::register($this);

$this->title = 'Закончить заказ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">

    <div class="col-md-6 qr-code-reader">
        <h2>Сканирование</h2>
        <div id="loadingMessage">🎥 Не удалось получить доступ к камере. (убедитесь, что Вы дали разрешение на использование камеры)</div>
        <canvas id="canvas" hidden></canvas>
    </div>

    <div class="col-md-6">
        <div class="order-form">
            <h2>Данные</h2>

            <?php $form = ActiveForm::begin(); ?>

            <?php $modelForm->id = Yii::$app->request->get('id') ? intval(Yii::$app->request->get('id')) : null;
                echo $form->field($modelForm, 'id')->textInput() ?>

            <?= $form->field($modelForm, 'scooter_state')->widget(Select2::classname(), [
                'data' => common\models\Order::getScooterStateList(),
                'options' => ['placeholder' => 'Выбрать'],
            ]); ?>

            <?= $form->field($modelForm, 'penalty_id')->widget(Select2::classname(), [
                'data' => common\models\Penalty::getPenaltyList(),
                'options' => ['placeholder' => 'Выбрать'],
            ]); ?>

            <?= $form->field($modelForm, 'comment')->textarea() ?>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>

</div>