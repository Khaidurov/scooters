<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Device */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="device-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'model')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'teh_status')->widget(Select2::classname(), [
        'data' => common\models\Device::getTehStatusList(),
        'options' => ['placeholder' => 'Выбрать'],
    ]); ?>

    <?= $form->field($model, 'issue_point_id')->widget(Select2::classname(), [
        'data' => common\models\Device::getIssuePointList(),
        'options' => ['placeholder' => 'Выбрать'],
    ]); ?>

    <?= $form->field($model, 'uses_count')->textInput() ?>

    <?= $form->field($model, 'device_type_id')->widget(Select2::classname(), [
        'data' => common\models\Device::getDeviceTypeList(),
        'options' => ['placeholder' => 'Выбрать'],
    ]); ?>

    <?= $form->field($model, 'status')->widget(Select2::classname(), [
        'data' => common\models\Device::getSatatusList(),
        'options' => ['placeholder' => 'Выбрать'],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
