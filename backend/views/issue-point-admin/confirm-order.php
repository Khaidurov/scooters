<?php

use yii\widgets\ActiveForm;
use backend\assets\QrReaderAsset;
use backend\assets\ConfirmOrderAsset;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $modelForm backend\models\form\ConfirmOrderForm */

QrReaderAsset::register($this);
ConfirmOrderAsset::register($this);

$this->title = 'Новый заказ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">

    <div class="col-md-6 qr-code-reader">
        <h2>Сканирование</h2>
        <div id="loadingMessage">🎥 Не удалось получить доступ к камере. (убедитесь, что Вы дали разрешение на использование камеры)</div>
        <canvas id="canvas" hidden></canvas>
    </div>

    <div class="col-md-6">
        <div class="order-form">
            <h2>Данные</h2>

            <?php $form = ActiveForm::begin(); ?>

            <?php $modelForm->id = Yii::$app->request->get('id') ? intval(Yii::$app->request->get('id')) : null;
                echo $form->field($modelForm, 'id')->textInput(); ?>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>

</div>