<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'scooter_state')->widget(Select2::classname(), [
        'data' => common\models\Order::getScooterStateList(),
        'options' => ['placeholder' => 'Выбрать'],
    ]); ?>

    <?= $form->field($model, 'penalty_id')->widget(Select2::classname(), [
        'data' => common\models\Penalty::getPenaltyList(),
        'options' => ['placeholder' => 'Выбрать'],
    ]); ?>

    <?= $form->field($model, 'comment')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
