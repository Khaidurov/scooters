<?php

use yii\widgets\Pjax;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $IssuePointDetail array */

$this->title = 'Точка выдачи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="device-index">


    <?php Pjax::begin(); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="table-responsive">
                        <?= DetailView::widget([
                            'model' => $IssuePointDetail,
                            'attributes' => [
                                'id',
                                'name',
                                'address',
                                'desc_full',
                                'desc_short',
                                [
                                    'attribute' => 'device_type_id',
                                    'value' => function($model) {
                                        return $model->deviceType->name;
                                    },
                                ],
                                [
                                    'attribute' => 'devices_count',
                                    'value' => function($model) {
                                        return  count($model->devices);
                                    },
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php Pjax::end(); ?>
</div>
