<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use nkizza\mappick\MapPick;

/* @var $this yii\web\View */
/* @var $model common\models\IssuePoint */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="issue-point-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?php
    echo $form->field($model, 'coordinates')->widget(
        'kolyunya\yii2\widgets\MapInputWidget',
        [
            'key' => 'AIzaSyDXKBEkl2Hw5o8KPuZE3iv_Gh3WfBXMaYY',
            'latitude' => Yii::$app->params['defaultLat'],
            'longitude' => Yii::$app->params['defaultLng'],
            'zoom' => 12,
            'height' => '400px',
            'pattern' => '%latitude%;%longitude%',
            'animateMarker' => true,
        ]
    );
    ?>

    <?= $form->field($model, 'desc_full')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'desc_short')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'admin_id')->widget(Select2::classname(), [
        'data' => common\modules\userprofile\models\UserProfile::getIssuePointAdminList(),
        'options' => ['placeholder' => 'Выбрать'],
    ]); ?>

    <?= $form->field($model, 'device_type_id')->widget(Select2::classname(), [
        'data' => common\models\IssuePoint::getDeviceTypeList(),
        'options' => ['placeholder' => 'Выбрать'],
    ]); ?>

    <?= $form->field($model, 'status')->widget(Select2::classname(), [
        'data' => common\models\IssuePoint::getSatatusList(),
        'options' => ['placeholder' => 'Выбрать'],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
