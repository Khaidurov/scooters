<?php

/* @var $this yii\web\View */
/* @var $model common\models\IssuePoint */

$this->title = 'Обновить точку выдачи: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Issue Points', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="issue-point-update">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $this->render('_form', [
                    'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
