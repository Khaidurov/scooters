<?php

/* @var $this yii\web\View */
/* @var $model common\models\IssuePoint */

$this->title = 'Создать точку выдачи';
$this->params['breadcrumbs'][] = ['label' => 'Issue Points', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="issue-point-create">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
