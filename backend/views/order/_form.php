<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'amount_price')->textInput() ?>

    <?= $form->field($model, 'admin_start_id')->textInput() ?>

    <?= $form->field($model, 'admin_end_id')->textInput() ?>

    <?= $form->field($model, 'client_id')->textInput() ?>

    <?= $form->field($model, 'tariff_id')->widget(Select2::classname(), [
        'data' => common\models\Order::getTariffList(),
        'options' => ['placeholder' => 'Выбрать'],
    ]); ?>

    <?= $form->field($model, 'issue_point_start_id')->widget(Select2::classname(), [
        'data' => common\models\Order::getIssuePointList(),
        'options' => ['placeholder' => 'Выбрать'],
    ]); ?>

    <?= $form->field($model, 'issue_point_end_id')->widget(Select2::classname(), [
        'data' => common\models\Order::getIssuePointList(),
        'options' => ['placeholder' => 'Выбрать'],
    ]); ?>

    <?= $form->field($model, 'live_status')->widget(Select2::classname(), [
        'data' => common\models\Order::getLiveStatusList(),
        'options' => ['placeholder' => 'Выбрать'],
    ]); ?>

    <?= $form->field($model, 'scooter_state')->widget(Select2::classname(), [
        'data' => common\models\Order::getScooterStateList(),
        'options' => ['placeholder' => 'Выбрать'],
    ]); ?>

    <?= $form->field($model, 'signature_img')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'comment')->textarea() ?>

    <?= $form->field($model, 'status')->widget(Select2::classname(), [
        'data' => common\models\Order::getSatatusList(),
        'options' => ['placeholder' => 'Выбрать'],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
