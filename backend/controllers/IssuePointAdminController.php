<?php

namespace backend\controllers;

use backend\models\form\ConfirmOrderForm;
use backend\models\form\ApproveOrderForm;
use common\models\IssuePoint;
use common\models\Order;
use common\models\search\OrderSearch;
use Yii;
use common\models\Device;
use common\models\search\DeviceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * IssuePointAdminController
 */
class IssuePointAdminController extends Controller
{
    /**
     * Index models.
     * @return mixed
     */
    public function actionIndex()
    {
        $IssuePointDetail = IssuePoint::getIssuePointDetailByAdmin(Yii::$app->user->id);

        return $this->render('index', [
            'IssuePointDetail' => $IssuePointDetail,
        ]);
    }

    /**
     * Lists all Device models.
     * @return mixed
     */
    public function actionDevices()
    {
        $searchModel = new DeviceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,  IssuePoint::getIssuePointByAdmin(Yii::$app->user->id));

        return $this->render('devices', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Device model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDeviceView($id)
    {
        return $this->render('device-view', [
            'model' => $this->findDeviceModel($id),
        ]);
    }

    /**
     * Updates an existing Device model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDeviceUpdate($id)
    {
        $model = $this->findDeviceModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['device-view', 'id' => $model->id]);
        }

        return $this->render('device-update', [
            'model' => $model,
        ]);
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionOrders()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,  IssuePoint::getIssuePointByAdmin(Yii::$app->user->id));

        return $this->render('orders', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionOrderView($id)
    {
        return $this->render('order-view', [
            'model' => $this->findOrderModel($id),
        ]);
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionOrderUpdate($id)
    {
        $model = $this->findOrderModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['order-view', 'id' => $model->id]);
        }

        return $this->render('order-update', [
            'model' => $model,
        ]);
    }

    /**
     * Index models.
     * @return mixed
     */
    public function actionConfirmOrder()
    {
        $modelForm = new ConfirmOrderForm();

        if ($modelForm->load(Yii::$app->request->post()) && $modelForm->validate()) {
            return $this->redirect(['order-view', 'id' => $modelForm->confirm()]);
        } else {
            return $this->render('confirm-order', [
                'modelForm' => $modelForm,
            ]);
        }

    }

    /**
     * Index models.
     * @return mixed
     */
    public function actionApproveOrder()
    {
        $modelForm = new ApproveOrderForm();
        $modelForm->scooter_state = Order::SCOOTER_STATE_OK;

        if ($modelForm->load(Yii::$app->request->post()) && $modelForm->validate()) {
            return $this->redirect(['order-view', 'id' => $modelForm->approve()]);
        } else {
            return $this->render('approve-order', [
                'modelForm' => $modelForm,
            ]);
        }

    }

    /**
     * Finds the Device model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Device the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findDeviceModel($id)
    {
        if (($model = Device::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findOrderModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
