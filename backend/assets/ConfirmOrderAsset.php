<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * ConfirmOrder asset bundle.
 */
class ConfirmOrderAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'js/confirm-order.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'backend\assets\AdminThemeAsset',
        'backend\assets\QrReaderAsset',
    ];
}
