<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * QrReader asset bundle.
 */
class QrReaderAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'js/jsQR.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'backend\assets\AdminThemeAsset',
    ];
}
