<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Admin theme asset bundle.
 */
class AdminThemeAsset extends AssetBundle
{
    public $sourcePath = '@app/admintheme/';
    public $css = [
        '//fonts.googleapis.com/css?family=Open+Sans:400,600%7CRubik:300,400,500%7CSource+Code+Pro',
        'vendor/loaders.css/loaders.min.css',
        'vendor/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css',
        'vendor/datatables/media/css/dataTables.bootstrap.min.css',
        'vendor/font-awesome/css/font-awesome.min.css',
        'vendor/iCheck/skins/square/_all.css',
        'vendor/mdi/css/materialdesignicons.min.css',
        'vendor/open-iconic/font/css/open-iconic-bootstrap.min.css',
        'vendor/sweetalert2/dist/sweetalert2.min.css',
        'vendor/toastr/toastr.min.css',

        'css/style.min.css',
    ];
    public $js = [
        'vendor/bootstrap-switch/dist/js/bootstrap-switch.min.js',
        'vendor/datatables/media/js/jquery.dataTables.min.js',
        'vendor/datatables/media/js/dataTables.bootstrap.min.js',
        'vendor/iCheck/icheck.min.js',
        'vendor/jquery-slimscroll/jquery.slimscroll.min.js',
        'vendor/sweetalert2/dist/sweetalert2.min.js',
        'vendor/toastr/toastr.min.js',

        'scripts/classtoggle.min.js',
        'scripts/collapsibleMenu.min.js',
        'scripts/colors.min.js',
        'scripts/init.min.js',
        'scripts/panel.min.js',
        'scripts/sidebar.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
