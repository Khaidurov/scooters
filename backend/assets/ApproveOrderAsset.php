<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * ApproveOrder asset bundle.
 */
class ApproveOrderAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'js/approve-order.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'backend\assets\AdminThemeAsset',
        'backend\assets\QrReaderAsset',
    ];
}
