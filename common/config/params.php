<?php
return [
    'adminEmail' => env('ADMIN_EMAIL'),
    'supportEmail' => env('SUPPORT_EMAIL'),
    'user.passwordResetTokenExpire' => 3600,
    'defaultLat' => env('DEFAULT_LAT'),
    'defaultLng' => env('DEFAULT_LNG')
];
