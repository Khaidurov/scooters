<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@uploadImg', dirname(dirname(__DIR__)) . '/frontend/web/img');
Yii::setAlias('@publicImg', env('BASE_URL') . '/frontend/web/img');

