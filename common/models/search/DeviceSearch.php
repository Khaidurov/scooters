<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Device;
use kartik\daterange\DateRangeBehavior;

/**
 * DeviceSearch represents the model behind the search form of `common\models\Device`.
 */
class DeviceSearch extends Device
{
    public $createTimeRange;
    public $createTimeStart;
    public $createTimeEnd;

    public $updateTimeRange;
    public $updateTimeStart;
    public $updateTimeEnd;

    public $createdByEmail;
    public $updatedByEmail;

    public function behaviors()
    {
        return [
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'createTimeRange',
                'dateStartAttribute' => 'createTimeStart',
                'dateEndAttribute' => 'createTimeEnd',
            ],
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'updateTimeRange',
                'dateStartAttribute' => 'updateTimeStart',
                'dateEndAttribute' => 'updateTimeEnd',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'teh_status', 'issue_point_id', 'uses_count', 'device_type_id', 'status'], 'integer'],
            [['name', 'model', 'createdByEmail', 'updatedByEmail'], 'safe'],
            [['createTimeRange', 'updateTimeRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $issue_point_id = null)
    {
        $query = Device::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => [ 'pageSize' => 10 ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            $this::tableName() . '.' . 'id' => $this->id,
            $this::tableName() . '.' . 'teh_status' => $this->teh_status,
            $this::tableName() . '.' . 'uses_count' => $this->uses_count,
            $this::tableName() . '.' . 'device_type_id' => $this->device_type_id,
            $this::tableName() . '.' . 'status' => $this->status,
        ]);

        if ($issue_point_id) {
            $query->andFilterWhere([
                $this::tableName() . '.' . 'issue_point_id' => $issue_point_id,
            ]);
        } else {
            $query->andFilterWhere([
                $this::tableName() . '.' . 'issue_point_id' => $this->issue_point_id,
            ]);
        }

        $query->andFilterWhere(['like', $this::tableName() . '.' . 'name', $this->name])
            ->andFilterWhere(['like', $this::tableName() . '.' . 'model', $this->model]);

        $query->timestamp(
            $this::tableName(),
            $this->createTimeStart,
            $this->createTimeEnd,
            $this->updateTimeStart,
            $this->updateTimeEnd
        );

        $query->blameable(
            $this->createdByEmail,
            $this->updatedByEmail
        );

        return $dataProvider;
    }
}
