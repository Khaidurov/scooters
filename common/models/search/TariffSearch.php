<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Tariff;
use kartik\daterange\DateRangeBehavior;

/**
 * TariffSearch represents the model behind the search form of `common\models\Tariff`.
 */
class TariffSearch extends Tariff
{
    public $createTimeRange;
    public $createTimeStart;
    public $createTimeEnd;

    public $updateTimeRange;
    public $updateTimeStart;
    public $updateTimeEnd;

    public $createdByEmail;
    public $updatedByEmail;

    public function behaviors()
    {
        return [
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'createTimeRange',
                'dateStartAttribute' => 'createTimeStart',
                'dateEndAttribute' => 'createTimeEnd',
            ],
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'updateTimeRange',
                'dateStartAttribute' => 'updateTimeStart',
                'dateEndAttribute' => 'updateTimeEnd',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'time', 'device_type_id', 'status', 'extra_minutes'], 'integer'],
            [['name', 'desc', 'createdByEmail', 'updatedByEmail'], 'safe'],
            [['price'], 'number'],
            [['createTimeRange', 'updateTimeRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tariff::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => [ 'pageSize' => 10 ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            $this::tableName() . '.' . 'id' => $this->id,
            $this::tableName() . '.' . 'time' => $this->time,
            $this::tableName() . '.' . 'price' => $this->price,
            $this::tableName() . '.' . 'device_type_id' => $this->device_type_id,
            $this::tableName() . '.' . 'status' => $this->status,
            $this::tableName() . '.' . 'extra_minutes' => $this->extra_minutes,
        ]);

        $query->andFilterWhere(['like', $this::tableName() . '.' .  'name', $this->name])
            ->andFilterWhere(['like', $this::tableName() . '.' .  'desc', $this->desc]);

        $query->timestamp(
            $this::tableName(),
            $this->createTimeStart,
            $this->createTimeEnd,
            $this->updateTimeStart,
            $this->updateTimeEnd
        );

        $query->blameable(
            $this->createdByEmail,
            $this->updatedByEmail
        );

        return $dataProvider;
    }
}
