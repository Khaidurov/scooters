<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Order;
use kartik\daterange\DateRangeBehavior;

/**
 * OrderSearch represents the model behind the search form of `common\models\Order`.
 */
class OrderSearch extends Order
{
    public $createTimeRange;
    public $createTimeStart;
    public $createTimeEnd;

    public $updateTimeRange;
    public $updateTimeStart;
    public $updateTimeEnd;

    public $createdByEmail;
    public $updatedByEmail;
    public $adminStartEmail;
    public $adminEndEmail;

    public $client;
    public $admin_start;
    public $admin_end;

    public function behaviors()
    {
        return [
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'createTimeRange',
                'dateStartAttribute' => 'createTimeStart',
                'dateEndAttribute' => 'createTimeEnd',
            ],
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'updateTimeRange',
                'dateStartAttribute' => 'updateTimeStart',
                'dateEndAttribute' => 'updateTimeEnd',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'date_start', 'date_end', 'time', 'client_id', 'tariff_id', 'issue_point_start_id', 'issue_point_end_id', 'live_status', 'scooter_state', 'status'], 'integer'],
            [['amount_price'], 'number'],
            [['signature_img', 'createdByEmail', 'updatedByEmail', 'adminStartEmail', 'adminEndEmail', 'admin_start', 'admin_end', 'client'], 'safe'],
            [['createTimeRange', 'updateTimeRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $issue_point_id = null)
    {
        $query = Order::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => [ 'pageSize' => 10 ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            $this::tableName() . '.' . 'id' => $this->id,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            $this::tableName() . '.' . 'time' => $this->time,
            $this::tableName() . '.' . 'amount_price' => $this->amount_price,
            $this::tableName() . '.' . 'client_id' => $this->client_id,
            $this::tableName() . '.' . 'tariff_id' => $this->tariff_id,
            $this::tableName() . '.' . 'issue_point_start_id' => $this->issue_point_start_id,
            $this::tableName() . '.' . 'issue_point_end_id' => $this->issue_point_end_id,
            $this::tableName() . '.' . 'live_status' => $this->live_status,
            $this::tableName() . '.' . 'scooter_state' => $this->scooter_state,
            $this::tableName() . '.' . 'status' => $this->status,
        ]);

        if ($issue_point_id) {
            $query->andWhere([
                'AND',
                ['OR',[
                    $this::tableName() . '.' . 'issue_point_start_id' => $issue_point_id,
                    $this::tableName() . '.' . 'issue_point_start_id' => $issue_point_id]
                ]
            ]);
        }

        $query->timestamp(
            $this::tableName(),
            $this->createTimeStart,
            $this->createTimeEnd,
            $this->updateTimeStart,
            $this->updateTimeEnd
        );

        $query->joinWith(['adminStart adminStart', 'adminEnd adminEnd'])
            ->andFilterWhere(['like', 'adminStart.login', $this->admin_start])
            ->andFilterWhere(['like', 'adminEnd.login', $this->admin_end]);

        $query->joinWith(['client client'])->andFilterWhere(['like', 'client.phone_number', $this->client]);

        return $dataProvider;
    }
}
