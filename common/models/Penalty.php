<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "penalty".
 *
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property string $name
 * @property string $desc
 * @property double $price
 * @property int $status
 */
class Penalty extends \common\models\SuperAcriveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'penalty';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'created_by', 'updated_by', 'status'], 'integer'],
            [['desc'], 'string'],
            [['price'], 'number'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'name' => 'Name',
            'desc' => 'Desc',
            'price' => 'Price',
            'status' => 'Status',
        ];
    }

    /**
     * @return array
     */
    public static function getPenaltyList()
    {
        $penaltyList = self::find()->active()->all();
        return ArrayHelper::map($penaltyList, 'id', 'name');
    }

    /**
     * {@inheritdoc}
     * @return PenaltyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PenaltyQuery(get_called_class());
    }
}
