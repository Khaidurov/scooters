<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "issue_point".
 *
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property string $name
 * @property string $address
 * @property double $lat
 * @property double $lng
 * @property string $desc_full
 * @property string $desc_short
 * @property int $admin_id
 * @property int $device_type_id
 * @property int $status
 * @property string $coordinates
 *
 * @property Device[] $devices
 * @property Yii::$app->user->identityClass $admin
 * @property Yii::$app->user->identityClass $createdBy
 * @property Yii::$app->user->identityClass $updatedBy
 * @property Order[] $orders
 * @property Order[] $orders0
 */
class IssuePoint extends \common\models\SuperAcriveRecord
{
    private $_coordinates;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'issue_point';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'created_by', 'updated_by', 'admin_id', 'device_type_id',  'status'], 'integer'],
            [['lat', 'lng'], 'number'],
            [['desc_full', 'coordinates'], 'string'],
            [['name', 'address', 'desc_short'], 'string', 'max' => 255],
            [['admin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Yii::$app->user->identityClass, 'targetAttribute' => ['admin_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => Yii::$app->user->identityClass, 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => Yii::$app->user->identityClass, 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
            'created_by' => 'Создал',
            'updated_by' => 'Обновил',
            'name' => 'Название',
            'address' => 'Адрес',
            'lat' => 'Lat',
            'lng' => 'Lng',
            'desc_full' => 'Описание полное',
            'desc_short' => 'Описание короткое',
            'admin_id' => 'Логин администратора',
            'device_type_id' => 'Тип устройства',
            'status' => 'Статус',
            'createdByEmail' => 'Создал',
            'updatedByEmail' => 'Обновил',
            'adminEmail' => 'Администратор',
            'devices_count' => 'Количество устройств',
            'coordinates' => 'Место на карте',
        ];
    }

    public function getCoordinates()
    {
        if ($this->lat !== null && $this->lng) {
            return $this->lat . ';' . $this->lng;
        } else {
            return null;
        }

    }

    public function setCoordinates($value)
    {
        $this->_coordinates = $value;

        if ($this->_coordinates !== null) {
            $coordinates = explode(";", $value);
            $this->lat = $coordinates[0];
            $this->lng = $coordinates[1];
        }
    }

    /**
     * @return array
     */
    public static function getDeviceTypeList()
    {
        $typeList = DeviceType::find()->all();
        return ArrayHelper::map($typeList, 'id', 'name');
    }

    /**
     * @return array
     * SELECT id,
     * ( 6371 * acos( cos( radians(43.866379) ) * cos( radians( lat ) ) * cos( radians( lng ) – radians(56.347038) ) + sin( radians(43.866379) ) * sin( radians( lat ) ) ) ) AS distance
     * FROM markers
     * HAVING distance < 25
     * ORDER BY distance
     * LIMIT 0 , 20;
     */
    public static function getIssuePointList($lat, $lng, $r)
    {
        return static::find()
            ->select([
                static::tableName() . '.' . 'id',
                static::tableName() . '.' . 'lat',
                static::tableName() . '.' . 'lng',
                static::tableName() . '.' . 'device_type_id',
                'COUNT(devices.id) AS devices_count',
            ])
            ->active(static::tableName())
            ->where(':dLat >= -:r AND :dLat <= :r AND :dLng >= -:r AND :dLng <= :r AND :dLat * :dLat + :dLng * :dLng <= :r * :r',
                [
                    ':dLat' => static::tableName() . '.' . 'lat' - $lat,
                    ':dLng' => static::tableName() . '.' . 'lng' - $lng,
                    ':r' => $r,
                ]
            )
            ->joinWith(['devices devices'], false)
            ->groupBy( static::tableName() . '.' . 'id')
            ->asArray()
            ->all();
    }

    /**
     * @return array
     */
    public static function getIssuePointDetail($id)
    {
        $issuePoint = static::find()
            ->select([
                static::tableName() . '.' . 'id',
                static::tableName() . '.' . 'name',
                static::tableName() . '.' . 'address',
                static::tableName() . '.' . 'desc_full',
                static::tableName() . '.' . 'desc_short',
                static::tableName() . '.' . 'device_type_id',
                static::tableName() . '.' . 'lat',
                static::tableName() . '.' . 'lng',
            ])
            ->where([
                static::tableName() . '.' . 'id' =>  $id,
            ])
            ->active(static::tableName())
            ->asArray()
            ->one();

        $devicesCount['devices_count'] = Device::find()->where([
            'issue_point_id' => $id,
            'status' => Device::STATUS_ACTIVE
        ])->count();

        return ArrayHelper::merge($issuePoint, $devicesCount);
    }

    /**
     * @return int
     */
    public static function getIssuePointByAdmin($admin_id)
    {
        return static::find()
            ->where([
                static::tableName() . '.' . 'admin_id' =>  $admin_id,
            ])
            ->one()->id;
    }

    /**
     * @return array
     */
    public static function getIssuePointDetailByAdmin($admin_id)
    {
        return static::find()
            ->select([
                static::tableName() . '.' . 'id',
                static::tableName() . '.' . 'name',
                static::tableName() . '.' . 'address',
                static::tableName() . '.' . 'desc_full',
                static::tableName() . '.' . 'desc_short',
                static::tableName() . '.' . 'device_type_id',
                static::tableName() . '.' . 'admin_id',
            ])
            ->where([
                static::tableName() . '.' . 'admin_id' =>  $admin_id
            ])
            ->active(static::tableName())
            ->one();
    }

    /**
     * @return string
     */
    public function getAdminEmail()
    {
        return $this->admin->email;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevices()
    {
        return $this->hasMany(Device::className(), ['issue_point_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin()
    {
        return $this->hasOne(Yii::$app->user->identityClass, ['id' => 'admin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Yii::$app->user->identityClass, ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(Yii::$app->user->identityClass, ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersEnd()
    {
        return $this->hasMany(Order::className(), ['issue_point_end_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersStart()
    {
        return $this->hasMany(Order::className(), ['issue_point_start_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeviceType()
    {
        return $this->hasOne(DeviceType::className(), ['id' => 'device_type_id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\IssuePointQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\IssuePointQuery(get_called_class());
    }
}
