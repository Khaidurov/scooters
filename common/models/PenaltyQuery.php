<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Penalty]].
 *
 * @see Penalty
 */
class PenaltyQuery extends \common\models\query\SuperActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Penalty[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Penalty|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
