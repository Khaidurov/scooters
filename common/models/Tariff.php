<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tariff".
 *
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property string $name
 * @property string $desc
 * @property int $time
 * @property int $extra_minutes
 * @property double $price
 * @property int $device_type_id
 * @property int $status
 *
 * @property Order[] $orders
 * @property DeviceType $deviceType
 * @property Yii::$app->user->identityClass $createdBy
 * @property Yii::$app->user->identityClass $updatedBy
 */
class Tariff extends \common\models\SuperAcriveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tariff';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'created_by', 'updated_by', 'time', 'device_type_id', 'status', 'extra_minutes'], 'integer'],
            [['desc'], 'string'],
            [['price'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['device_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DeviceType::className(), 'targetAttribute' => ['device_type_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => Yii::$app->user->identityClass, 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => Yii::$app->user->identityClass, 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
            'created_by' => 'Создал',
            'updated_by' => 'Обновил',
            'name' => 'Название',
            'desc' => 'Описание',
            'time' => 'Время',
            'price' => 'Цена',
            'device_type_id' => 'Тип устройства',
            'status' => 'Статус',
            'createdByEmail' => 'Создал',
            'updatedByEmail' => 'Обновил',
            'extra_minutes' => 'Дополнительное время'
        ];
    }

    /**
     * @return array
     */
    public static function getDeviceTypeList()
    {
        $typeList = DeviceType::find()->all();
        return ArrayHelper::map($typeList, 'id', 'name');
    }

    /**
     * @return array
     */
    public static function getTariffsByDeviceType($device_type_id)
    {
        return static::find()
            ->select([
                'id',
                'name',
                'desc',
                'time',
                'price'
            ])
            ->where(['device_type_id' => $device_type_id])
            ->active()
            ->all();
    }

    /**
     * @return array
     */
    public static function getTariffById($id)
    {
        return static::find()
            ->select([
                'id',
                'name',
                'desc',
                'time',
                'extra_minutes',
                'price',
            ])
            ->where(['id' => $id])
            ->active()
            ->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['tariff_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeviceType()
    {
        return $this->hasOne(DeviceType::className(), ['id' => 'device_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Yii::$app->user->identityClass, ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(Yii::$app->user->identityClass, ['id' => 'updated_by']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\TariffQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\TariffQuery(get_called_class());
    }
}
