<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "sms_notification".
 *
 * @property int $user_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $sms_count
 */
class SmsNotification extends \yii\db\ActiveRecord
{
    const MAX_SMS_COUNT = 10;
    const TIME_Limit = 60; //in second

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sms_notification';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'sms_count'], 'integer'],
            [['sms_count'], 'default', 'value' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'sms_count' => 'Sms Count',
        ];
    }

    public function canSend()
    {
        if ( (time() - $this->updated_at < self::TIME_Limit) || $this->sms_count > self::MAX_SMS_COUNT ) {
            return false;
        }

        return true;
    }

    public function incrementSmsCount () {

        $this->sms_count++;
        $this->save();

    }

    public function clearSmsCount () {

        $this->sms_count = 0;
        $this->save();

    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\SmsNotificationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\SmsNotificationQuery(get_called_class());
    }
}
