<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "device".
 *
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property string $name
 * @property string $model
 * @property int $teh_status
 * @property int $issue_point_id
 * @property int $uses_count
 * @property int $device_type_id
 * @property int $status
 *
 * @property DeviceType $deviceType
 * @property IssuePoint $issuePoint
 * @property Yii::$app->user->identityClass $createdBy
 * @property Yii::$app->user->identityClass $updatedBy
 */
class Device extends \common\models\SuperAcriveRecord
{
    const STATUS_BUSY = 2;

    const TEH_STATUS_CRASH = 0;
    const TEH_STATUS_OK = 1;
    const TEH_STATUS_REPAIR = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'created_by', 'updated_by', 'teh_status', 'issue_point_id', 'uses_count', 'device_type_id', 'status'], 'integer'],
            [['name', 'model'], 'string', 'max' => 255],
            [['device_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DeviceType::className(), 'targetAttribute' => ['device_type_id' => 'id']],
            [['issue_point_id'], 'exist', 'skipOnError' => true, 'targetClass' => IssuePoint::className(), 'targetAttribute' => ['issue_point_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => Yii::$app->user->identityClass, 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => Yii::$app->user->identityClass, 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
            'created_by' => 'Создал',
            'updated_by' => 'Обновил',
            'name' => 'Название',
            'model' => 'Модель',
            'teh_status' => 'Тех. Статус',
            'issue_point_id' => 'Точка выдачи',
            'uses_count' => 'Количество использований',
            'device_type_id' => 'Тип устройства',
            'status' => 'Статус',
            'createdByEmail' => 'Создал',
            'updatedByEmail' => 'Обновил'
        ];
    }

    /**
     * @return array
     */
    public static function getSatatusList()
    {
        return array(
            self::STATUS_NON_ACTIVE => 'Не активен',
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_BUSY => 'Занят',
        );
    }

    /**
     * @return int
     */
    public static function getDeviceMinUses($issue_point_id)
    {
        $subquery = static::find()->active()
            ->where([
                'issue_point_id' => $issue_point_id,
            ])->min('uses_count');

        return static::find()
            ->where([
                'uses_count' =>  $subquery,
            ])->one()->id;
    }

    /**
     * @return boolean
     */
    public static function afterEndOrder($id, $issue_point_id)
    {
        $model = static::findOne($id);
        $model->uses_count++;
        $model->status = Device::STATUS_ACTIVE;
        $model->issue_point_id = $issue_point_id;

        if ($model->validate() && $model->save()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return boolean
     */
    public static function setStatus($id, $status)
    {
        $model = static::findOne($id);
        $model->status = $status;

        if ($model->validate() && $model->save()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return boolean
     */
    public static function setIssuePointAndStatus($id, $issue_point, $status)
    {
        $model = static::findOne($id);
        $model->status = $status;
        $model->issue_point_id = $issue_point;

        if ($model->validate() && $model->save()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return array
     */
    public static function getDeviceTypeList()
    {
        $typeList = DeviceType::find()->all();
        return ArrayHelper::map($typeList, 'id', 'name');
    }

    /**
     * @return array
     */
    public static function getIssuePointList()
    {
        $typeList = IssuePoint::find()->all();
        return ArrayHelper::map($typeList, 'id', 'name');
    }

    /**
     * @return array
     */
    public static function getTehStatusList()
    {
        return array(
            self::TEH_STATUS_CRASH => 'Сломался',
            self::TEH_STATUS_OK => 'Исправен',
            self::TEH_STATUS_REPAIR => 'На ремонте',
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeviceType()
    {
        return $this->hasOne(DeviceType::className(), ['id' => 'device_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIssuePoint()
    {
        return $this->hasOne(IssuePoint::className(), ['id' => 'issue_point_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Yii::$app->user->identityClass, ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(Yii::$app->user->identityClass, ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['device_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\DeviceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\DeviceQuery(get_called_class());
    }
}
