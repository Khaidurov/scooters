<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "device_type".
 *
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property string $name
 * @property string $desc
 * @property string $img
 * @property int $status
 *
 * @property Device[] $devices
 * @property Yii::$app->user->identityClass $createdBy
 * @property Yii::$app->user->identityClass $updatedBy
 * @property Tariff[] $tariffs
 */
class DeviceType extends \common\models\SuperAcriveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device_type';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'created_by', 'updated_by', 'status'], 'integer'],
            [['desc'], 'string'],
            [['name', 'img'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' =>Yii::$app->user->identityClass, 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => Yii::$app->user->identityClass, 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
            'created_by' => 'Создал',
            'updated_by' => 'Обновил',
            'name' => 'Название',
            'desc' => 'Описание',
            'img' => 'Изображение',
            'status' => 'Статус',
            'createdByEmail' => 'Создал',
            'updatedByEmail' => 'Обновил'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevices()
    {
        return $this->hasMany(Device::className(), ['device_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIssuePoints()
    {
        return $this->hasMany(IssuePoint::className(), ['device_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Yii::$app->user->identityClass, ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(Yii::$app->user->identityClass, ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffs()
    {
        return $this->hasMany(Tariff::className(), ['device_type_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\DeviceTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\DeviceTypeQuery(get_called_class());
    }
}
