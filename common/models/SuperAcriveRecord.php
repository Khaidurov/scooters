<?php

namespace common\models;

class SuperAcriveRecord extends \yii\db\ActiveRecord {

    const STATUS_NON_ACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @return string
     */
    public function getCreatedByEmail()
    {
        return $this->createdBy->email;
    }

    /**
     * @return string
     */
    public function getUpdatedByEmail()
    {
        return $this->updatedBy->email;
    }

    /**
     * @return array
     */
    public static function getSatatusList()
    {
        return array(
            self::STATUS_NON_ACTIVE => 'Не активен',
            self::STATUS_ACTIVE => 'Активен'
        );
    }
}