<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\modules\user\models\User;

/**
 * Notification Model
 */
class Notification extends Model
{

    public static function SendCode($phone_number){

        $userModel = User::findByPhoneNumber($phone_number);

        $smsNotificationModel = SmsNotification::findOne($userModel->id);

        if ($smsNotificationModel->canSend()) {

            $smsNotificationModel->incrementSmsCount();
            return $phone_number;

        } else {

            return false;

        }


    }

    public static function CodeValidation($id, $code){

        if($code == "0000") {

            $smsNotificationModel = SmsNotification::findOne($id);
            $smsNotificationModel->clearSmsCount();

            return true;

        } else {

            return false;

        }
    }

    public static function NewUserValidation($id){
        return true;
    }

}