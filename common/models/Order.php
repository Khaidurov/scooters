<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $date_start
 * @property int $date_end
 * @property int $time
 * @property double $amount_price
 * @property int $admin_start_id
 * @property int $admin_end_id
 * @property int $client_id
 * @property int $tariff_id
 * @property int $issue_point_start_id
 * @property int $issue_point_end_id
 * @property int $live_status
 * @property int $device_id
 * @property int $scooter_state
 * @property int $penalty_id
 * @property string $signature_img
 * @property string $comment
 * @property string $bank_orderId
 * @property int $status
 *
 * @property IssuePoint $issuePointEnd
 * @property IssuePoint $issuePointStart
 * @property Tariff $tariff
 * @property Yii::$app->user->identityClass $adminEnd
 * @property Yii::$app->user->identityClass $adminStart
 * @property Yii::$app->user->identityClass $client
 * @property Yii::$app->user->identityClass $createdBy
 * @property Yii::$app->user->identityClass $updatedBy
 */
class Order extends \common\models\SuperAcriveRecord
{
    const LIVE_STATUS_BOOKING = 0;
    const LIVE_STATUS_LEASE = 1;
    const LIVE_STATUS_APPROVED = 2;
    const LIVE_STATUS_PAID = 3;
    const LIVE_CANCELED = 4;

    const SCOOTER_STATE_CRASH = 0;
    const SCOOTER_STATE_OK = 1;
    const SCOOTER_STATE_MISS = 2;

    const STATUS_INITIAL = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'created_by', 'updated_by', 'date_start', 'date_end', 'time', 'admin_start_id', 'admin_end_id', 'client_id', 'tariff_id', 'issue_point_start_id', 'issue_point_end_id', 'live_status', 'scooter_state', 'status', 'device_id', 'penalty_id'], 'integer'],
            [['amount_price'], 'number'],
            [['signature_img', 'comment', 'bank_orderId'], 'string', 'max' => 255],
            [['issue_point_end_id'], 'exist', 'skipOnError' => true, 'targetClass' => IssuePoint::className(), 'targetAttribute' => ['issue_point_end_id' => 'id']],
            [['issue_point_start_id'], 'exist', 'skipOnError' => true, 'targetClass' => IssuePoint::className(), 'targetAttribute' => ['issue_point_start_id' => 'id']],
            [['tariff_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tariff::className(), 'targetAttribute' => ['tariff_id' => 'id']],
            [['admin_end_id'], 'exist', 'skipOnError' => true, 'targetClass' => Yii::$app->user->identityClass, 'targetAttribute' => ['admin_end_id' => 'id']],
            [['admin_start_id'], 'exist', 'skipOnError' => true, 'targetClass' => Yii::$app->user->identityClass, 'targetAttribute' => ['admin_start_id' => 'id']],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Yii::$app->user->identityClass, 'targetAttribute' => ['client_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => Yii::$app->user->identityClass, 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => Yii::$app->user->identityClass, 'targetAttribute' => ['updated_by' => 'id']],
            [['penalty_id'], 'exist', 'skipOnError' => true, 'targetClass' => Penalty::className(), 'targetAttribute' => ['penalty_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
            'created_by' => 'Создал',
            'updated_by' => 'Обновил',
            'date_start' => 'Дата начала',
            'date_end' => 'Дата окончания',
            'time' => 'Время',
            'amount_price' => 'Стоимость',
            'admin_start_id' => 'ID администратора старта',
            'admin_end_id' => 'ID администратора окончания',
            'client_id' => 'Клиент',
            'tariff_id' => 'Тариф',
            'issue_point_start_id' => 'Точка начала',
            'issue_point_end_id' => 'Точка окончания',
            'live_status' => 'Текущий статус',
            'scooter_state' => 'Состояние устройства',
            'signature_img' => 'Изображение подписи',
            'status' => 'Статус',
            'createdByEmail' => 'Создал',
            'updatedByEmail' => 'Обновил',
            'adminStartEmail' => 'Администратор старта',
            'adminEndEmail' => 'Администратор окончания',
            'device_id' => 'ID устройства',
            'comment' => 'Комментарий',
            'admin_start' => 'Администратор старта',
            'admin_end' => 'Администратор окончания',
            'client' => 'Клиент',
            'penalty_id' => 'Штраф',
        ];
    }

    /**
     * @return int
     */
    public static function getSatatus($id)
    {
        return static::findOne($id)->live_status;
    }

    /**
     * @return int
     */
    public static function setStatus($id, $status)
    {
        $model = static::findOne($id);
        $model->live_status = $status;

        if ($model->validate() && $model->save()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return array
     */
    public static function getSatatusList()
    {
        return array(
            self::STATUS_NON_ACTIVE => 'Не активен',
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_INITIAL => 'Начальный',
        );
    }

    /**
     * @return string
     */
    public function getAdminStartEmail()
    {
        return $this->adminStart->email;
    }

    /**
     * @return string
     */
    public function getAdminEndEmail()
    {
        return $this->adminEnd->email;
    }

    /**
     * @return array
     */
    public static function getTariffList()
    {
        $typeList = Tariff::find()->all();
        return ArrayHelper::map($typeList, 'id', 'name');
    }

    /**
     * @return array
     */
    public static function getIssuePointList()
    {
        $typeList = IssuePoint::find()->all();
        return ArrayHelper::map($typeList, 'id', 'name');
    }

    /**
     * @return array
     */
    public static function getLiveStatusList()
    {
        return array(
            self::LIVE_STATUS_BOOKING => 'Бронь',
            self::LIVE_STATUS_LEASE => 'В аренде',
            self::LIVE_STATUS_APPROVED => 'Принят администратором',
            self::LIVE_STATUS_PAID => 'Оплачен',
            self::LIVE_CANCELED => 'Отменен',
        );
    }

    /**
     * @return array
     */
    public static function getScooterStateList()
    {
        return array(
            self::SCOOTER_STATE_CRASH => 'Сломали',
            self::SCOOTER_STATE_OK => 'Исправен',
            self::SCOOTER_STATE_MISS => 'Не вернули',
        );
    }

    /**
     * @return array
     */
    public static function getOrderById($id)
    {
        return static::find()
            ->where(['id' => $id])
            ->active()
            ->one();
    }

    /**
     * @return array
     */
    public static function getOrdersByUser($user_id)
    {
        return static::find()
            ->where(['client_id' => $user_id])
            ->active()
            ->all();
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIssuePointEnd()
    {
        return $this->hasOne(IssuePoint::className(), ['id' => 'issue_point_end_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIssuePointStart()
    {
        return $this->hasOne(IssuePoint::className(), ['id' => 'issue_point_start_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(Tariff::className(), ['id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdminEnd()
    {
        return $this->hasOne(Yii::$app->user->identityClass, ['id' => 'admin_end_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdminStart()
    {
        return $this->hasOne(Yii::$app->user->identityClass, ['id' => 'admin_start_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Yii::$app->user->identityClass, ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Yii::$app->user->identityClass, ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(Yii::$app->user->identityClass, ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevice()
    {
        return $this->hasOne(Device::className(), ['id' => 'device_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenalty()
    {
        return $this->hasOne(Penalty::className(), ['id' => 'penalty_id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\OrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\OrderQuery(get_called_class());
    }
}
