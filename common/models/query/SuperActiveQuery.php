<?php

namespace common\models\query;

class SuperActiveQuery extends \yii\db\ActiveQuery
{
    public function active($table_name = null)
    {
        return $table_name ? $this->andWhere($table_name . '.' . '[[status]]=1') : $this->andWhere('[[status]]=1');
    }

    public function timestamp($table_name, $createTimeStart, $createTimeEnd, $updateTimeStart, $updateTimeEnd)
    {
        return $this->andFilterWhere(['>=', $table_name . '.' . 'created_at', $createTimeStart])
            ->andFilterWhere(['<', $table_name . '.' . 'created_at', $createTimeEnd])
            ->andFilterWhere(['>=', $table_name . '.' . 'updated_at', $updateTimeStart])
            ->andFilterWhere(['<', $table_name . '.' . 'updated_at', $updateTimeEnd]);
    }

    public function blameable($createdByEmail, $updatedByEmail)
    {
        return $this->joinWith(['createdBy creator', 'updatedBy updater'])
            ->andFilterWhere(['like', 'creator.email', $createdByEmail])
            ->andFilterWhere(['like', 'updater.email', $updatedByEmail]);
    }
}