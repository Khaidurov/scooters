<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\Tariff]].
 *
 * @see \common\models\Tariff
 */
class TariffQuery extends \common\models\query\SuperActiveQuery
{
    /**
     * {@inheritdoc}
     * @return \common\models\Tariff[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\Tariff|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
