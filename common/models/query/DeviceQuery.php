<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\Device]].
 *
 * @see \common\models\Device
 */
class DeviceQuery extends \common\models\query\SuperActiveQuery
{
    /**
     * {@inheritdoc}
     * @return \common\models\Device[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\Device|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
