<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\IssuePoint]].
 *
 * @see \common\models\IssuePoint
 */
class IssuePointQuery extends \common\models\query\SuperActiveQuery
{
    /**
     * {@inheritdoc}
     * @return \common\models\IssuePoint[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\IssuePoint|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
