<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\DeviceType]].
 *
 * @see \common\models\DeviceType
 */
class DeviceTypeQuery extends \common\models\query\SuperActiveQuery
{
    /**
     * {@inheritdoc}
     * @return \common\models\DeviceType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\DeviceType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
