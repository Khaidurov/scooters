<?php

namespace common\modules\api;

/**
 * api module definition class
 */
class Api extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'common\modules\api\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        \Yii::$app->user->enableSession = false;

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        \Yii::$app->request->enableCsrfValidation = false;

        // custom initialization code goes here
    }
}
