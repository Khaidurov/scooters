<?php

namespace common\modules\api\controllers;

use common\modules\api\models\form\CodeValidationForm;
use common\modules\api\models\form\NotificationRepeatForm;
use common\modules\api\models\form\PaymentBind;
use common\modules\api\models\form\SetAppTokenForm;
use common\modules\api\models\form\SetBankCardForm;
use common\modules\api\models\form\SetPhoneForm;
use common\modules\api\models\form\SetSignatureForm;
use common\modules\api\models\form\SetUserDataForm;
use common\modules\user\models\User;
use Yii;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use common\modules\userprofile\models\UserProfile;
use yii\web\UploadedFile;
use common\models\Notification;

/**
 * SignUp controller for the `api` module
 */
class SignUpController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'only' => [
                'get-status',
                'set-signature',
                'set-passport',
                'init-payment-bind',
                'end-payment-bind',
                'set-user-data',
                'set-app-token',
                'get-user-data'
            ],
        ];
        return $behaviors;
    }

    /**
     * @return array
     */
    public function actionGetStatus()
    {
        return array(
            'status' => UserProfile::getSatatus()
        );
    }

    /**
     * @return array
     */
    public function actionSetPhone()
    {

        $modelForm = new SetPhoneForm();

        if ($modelForm->load(Yii::$app->request->post(), '') && $modelForm->validate()) {

            if ($modelForm->checkByPhoneNumber()) {

                return $modelForm->getExistUser();

            } else {

                return $modelForm->signUp();

            }

        } else {

            return array(
                'error' => "Ошибка! Телефон введен неверно."
            );

        }

    }

    /**
     * @return array
     */
    public function actionNotificationRepeat()
    {
        $modelForm = new NotificationRepeatForm();

        if ($modelForm->load(Yii::$app->request->post(), '') && $modelForm->validate()) {

            return $modelForm->sendCode();

        } else {

            return array(
                'error' => "Ошибка! Вы не ввели свой номер телефона при регистрации."
            );

        }

    }

    /**
     * @return array
     */
    public function actionCodeValidation()
    {
        $modelForm = new CodeValidationForm();

        if ($modelForm->load(Yii::$app->request->post(), '') && $modelForm->validate()) {

            return $modelForm->codeValidation();

        } else {

            return array(
                'error' => "Ошибка! Вы не ввели свой номер телефона, либо введен неверный код подтверждения."
            );

        }

    }

    /**
     * @return array
     */
    public function actionInitPaymentBind()
    {
        $modelForm = new PaymentBind();

        $result = $modelForm->initPayment();

        if ($result) {

             return array(
                 'url' => $result

             );

        } else {

            return array(
                'error' => 'Не удалось связаться с банком. Попробуйте позже.'
            );
            
        }

    }

    /**
     * @return array
     */
    public function actionEndPaymentBind()
    {
        $modelForm = new PaymentBind();

        $result = $modelForm->endPayment();

        if ($result) {

            return array(
                'bindingId' => $result

            );

        } else {

            return array(
                'error' => 'Не удалось произвести тестовую оплату по вашей карте.'
            );

        }

    }

    /**
     * @return array
     */
    public function actionSetSignature()
    {
        $modelForm = new SetSignatureForm();

        if ($modelForm->load(Yii::$app->request->post(), '') && $modelForm->validate()) {

            return $modelForm->setSignature();

        } else {

            return array(
                'error' => "Ошибка! Данные введены неверно."
            );

        }
    }

    /**
     * @return array
     */
    public function actionSetPassport()
    {

        if (!UploadedFile::getInstanceByName('filename') || !Yii::$app->request->post('picture')) {

            return array(
                'error' => "Ошибка! Неверный формат файла.",
            );

        } else {

            $filename = UploadedFile::getInstanceByName('filename');
            $picture = Yii::$app->request->post('picture');


            if (UserProfile::setPassport($filename, $picture)) {

                return array(
                    'id' => Yii::$app->user->id,
                );

            } else {

                return array(
                    'error' => "Не удалось загрузить паспорт"
                );

            }

        }

    }

    /**
     * @return array
     */
    public function actionSetUserData()
    {
        $modelForm = new SetUserDataForm();

        if ($modelForm->load(Yii::$app->request->post(), '') && $modelForm->validate()) {

            return $modelForm->setUserData();

        } else {
            
            $errors = "Ошибка! ";
            foreach ($modelForm->errors as $error) {
                $errors .= $error[0] . " ";
            }

            return array(
                'error' => $errors 
            );

        }

    }

    /**
     * @return array
     */
    public function actionSetAppToken()
    {
        $modelForm = new SetAppTokenForm();

        if ($modelForm->load(Yii::$app->request->post(), '') && $modelForm->validate()) {

            return $modelForm->setAppToken();

        } else {

            return array(
                'error' => "Ошибка! Данные введены неверно."
            );

        }

    }

    /**
     * @return array
     */
    public function actionGetUserData()
    {
        $email = array(
            'email' => User::getEmail()
        );

        $userData = UserProfile::getUserData();
        return ArrayHelper::merge($email, $userData);

    }

}
