<?php

namespace common\modules\api\controllers;

use common\modules\api\models\form\IssuePointDetailForm;
use common\modules\api\models\form\IssuePointListForm;
use Yii;
use yii\web\Controller;
use common\models\IssuePoint;
use common\models\Tariff;

/**
 * Map controller for the `api` module
 */
class MapController extends Controller
{
    /**
     * @return array
     */
    public function actionGetIssuePoints()
    {
        $modelForm = new IssuePointListForm();

        if ($modelForm->load(Yii::$app->request->post(), '') && $modelForm->validate()) {

            $items = IssuePoint::getIssuePointList($modelForm->lat, $modelForm->lng, $modelForm->r);

            if (!$items) {

                return array(
                    'error' => 'Не удалось найти ничего поблизости',
                );

            } else {

                return array(
                    'items' => $items,
                );

            }

        } else {

            return array(
                'error' => "Ошибка! Не удалось получить Ваши координаты."
            );

        }

    }

    /**
     * @return array
     */
    public function actionGetIssuePointDetail()
    {

        $modelForm = new IssuePointDetailForm();

        if ($modelForm->load(Yii::$app->request->post(), '') && $modelForm->validate()) {

            return $modelForm->getDetailIssuePoint();

        } else {

            return array(
                'error' => "Ошибка! Точка выдачи не найдена."
            );

        }

    }
}
