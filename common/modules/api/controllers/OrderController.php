<?php

namespace common\modules\api\controllers;

use common\modules\api\models\form\OrderCancelForm;
use common\modules\api\models\form\OrderConfirmForm;
use common\modules\api\models\form\OrderDetailForm;
use common\modules\api\models\form\OrderGetStatusForm;
use common\modules\api\models\form\OrderNewForm;
use common\modules\api\models\form\OrderPriceForm;
use common\modules\api\models\form\PaymentBind;
use common\modules\api\models\form\TariffDetailForm;
use common\modules\api\models\form\UserOrdersForm;
use common\modules\user\models\User;
use Yii;
use yii\web\Controller;
use common\models\Order;
use yii\filters\auth\QueryParamAuth;
use yii\filters\AccessControl;

/**
 * Order controller for the `api` module
 */
class OrderController extends Controller
{

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'only' => [
                'get-status',
                'new',
                'cancel',
                'confirm',
                'approve',
                'paid',
                'get-order',
                'get-user-orders',
                'get-order-price'
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'except' => ['get-tariff', 'get-order', 'get-user-orders'],
            'rules' => [
                [
                    'allow' => true,
                    'matchCallback' => function ($rule, $action) {
                        if (Yii::$app->user->identity->status == User::STATUS_ACTIVE) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                ]
            ]
        ];
        return $behaviors;
    }

    /**
     * @return array
     */
    public function actionGetStatus()
    {
        $modelForm = new OrderGetStatusForm();

        if ($modelForm->load(Yii::$app->request->post(), '') && $modelForm->validate()) {

            return array(
                'status' => Order::getSatatus($modelForm->id)
            );

        } else {

            return array(
                'error' => "Ошибка! Заказ не найден."
            );

        }

    }

    /**
     * @return array
     */
    public function actionNew()
    {
        $modelForm = new OrderNewForm();

        if ($modelForm->load(Yii::$app->request->post(), '') && $modelForm->validate()) {

            return $modelForm->createOrder();

        } else {

            return array(
                'error' => "Ошибка! Точка выдачи, либо тариф не найдены."
            );

        }

    }

    /**
     * @return array
     */
    public function actionCancel()
    {
        $modelForm = new OrderCancelForm();

        if ($modelForm->load(Yii::$app->request->post(), '') && $modelForm->validate()) {

            return $modelForm->cancel();

        } else {

            return array(
                'error' => "Ошибка! Заказ не найден."
            );

        }

    }

    /**
     * @return array
     */
    public function actionConfirm()
    {

        $modelForm = new OrderConfirmForm();

        if ($modelForm->load(Yii::$app->request->post(), '') && $modelForm->validate()) {

            return $modelForm->confirm();

        } else {

            return array(
                'error' => "Ошибка! Заказ не найден."
            );

        }

    }

//    /**
//     * @return array
//     */
//    public function actionApprove()
//    {
//        $modelForm = new OrderApproveForm();
//
//        if ($modelForm->load(Yii::$app->request->post(), '') && $modelForm->validate()) {
//
//            return $modelForm->approve();
//
//        } else {
//
//            return array(
//                'error' => "Ошибка! Заказ, либо точка выдачи найдены."
//            );
//
//        }
//
//    }

    /**
     * @return array
     */
    public function actionPaid()
    {
        $modelForm = new PaymentBind();

        if ($modelForm->load(Yii::$app->request->post(), '') && $modelForm->validate()) {

            return $modelForm->payWithBind();

        } else {

            return array(
                'error' => "Ошибка! Заказ не найден."
            );

        }

    }

    /**
     * @return array
     */
    public function actionGetTariff()
    {
        $modelForm = new TariffDetailForm();

        if ($modelForm->load(Yii::$app->request->post(), '') && $modelForm->validate()) {

            return $modelForm->getTariffDetail();

        } else {

            return array(
                'error' => "Ошибка! Тариф не найден."
            );

        }

    }

    /**
     * @return array
     */
    public function actionGetOrder()
    {
        $modelForm = new OrderDetailForm();

        if ($modelForm->load(Yii::$app->request->post(), '') && $modelForm->validate()) {

            return $modelForm->getOrderDetail();

        } else {

            return array(
                'error' => "Ошибка! Заказ не найден."
            );

        }

    }

    /**
     * @return array
     */
    public function actionGetUserOrders()
    {
        $modelForm = new UserOrdersForm();

        $result = $modelForm->getUserOrders();

        if ($result) {

            return $result;

        } else {

            return array(
                'error' => 'Не удалось связаться с банком. Попробуйте позже.'
            );

        }

    }

    /**
     * @return array
     */
    public function actionGetOrderPrice()
    {
        $modelForm = new OrderPriceForm();

        if ($modelForm->load(Yii::$app->request->post(), '') && $modelForm->validate()) {

            return $modelForm->getPrice();

        } else {

            return array(
                'error' => "Ошибка! Заказ не найден."
            );

        }

    }
}
