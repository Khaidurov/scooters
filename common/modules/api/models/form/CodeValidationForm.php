<?php
namespace  common\modules\api\models\form;

use common\models\Notification;
use common\modules\user\models\User;
use common\modules\userprofile\models\UserProfile;
use Yii;
use yii\base\Model;

/**
 * CodeValidation form
 */
class CodeValidationForm extends Model
{
    public $id;
    public $code;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'code'], 'trim'],
            [['id', 'code'], 'required'],
            [['id'], 'integer'],
            [['code'], 'string', 'length' => [4,4]],
            [['id'], 'exist', 'targetClass' => User::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    public function codeValidation()
    {
        if (Notification::CodeValidation($this->id, $this->code)) {

            if (in_array(UserProfile::findOne($this->id)->status, UserProfile::getUserExistStatuses()) == false) {
                UserProfile::setSatatusById($this->id, UserProfile::STATUS_PHONE_OK);
            }

            return array(
                'access_token' => User::findIdentity($this->id)->access_token
            );

        } else {

            return array(
                'error' => "Код введен неверно"
            );

        }
    }

}