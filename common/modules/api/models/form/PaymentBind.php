<?php
namespace  common\modules\api\models\form;

use common\models\Order;
use common\modules\userprofile\models\UserProfile;
use Yii;
use yii\base\Model;
use Voronkovich\SberbankAcquiring\Client;
use Voronkovich\SberbankAcquiring\Currency;
use Voronkovich\SberbankAcquiring\OrderStatus;

/**
 * PaymentBind form
 */
class PaymentBind extends Model
{
    public $id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'trim'],
            [['id'], 'required'],
            [['id'], 'integer'],
            [['id'], 'exist', 'targetClass' => Order::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    public function initPayment()
    {
        $model = new Order();

        $model->client_id = Yii::$app->user->id;
        $model->status = Order::STATUS_INITIAL;


        if ($model->validate() && $model->save()) {
            $client = $this->initClientObject();

            $clientId = Yii::$app->user->id;
            $orderId = $model->id;
            $orderAmount = 100;
            $returnUrl = env('BASE_URL')  . env('SBER_SUCCESS_URL');

            $params['failUrl'] = env('BASE_URL')  . env('SBER_FAILURE_URL');
            $params['clientId'] = $clientId;

            $result = $client->registerOrder($orderId, $orderAmount, $returnUrl, $params);

            if (isset($result['orderId']) && !empty($result['orderId'])) {
                $model->bank_orderId = $result['orderId'];
                if ($model->validate() && $model->save()) {
                    return $result['formUrl'];
                }
            }

        }

        return false;

    }

    public function endPayment()
    {
        $model = Order::find()->where([
            'client_id' => Yii::$app->user->id,
            'status' => Order::STATUS_INITIAL
        ])->orderBy(['id' => SORT_DESC])->one();

        $client = $this->initClientObject();

        $result = $client->getOrderStatus($model->bank_orderId);

        if (OrderStatus::isDeposited($result['orderStatus'])) {

            $userProfileModel = UserProfile::findOne(Yii::$app->user->id);
            $userProfileModel->bank_billingId = $result['bindingInfo']['bindingId'];
            $userProfileModel->status = UserProfile::STATUS_CARD;

            if ($userProfileModel->validate() && $userProfileModel->save()) {

                $client->refundOrder($model->bank_orderId, 100);

                return $userProfileModel->bank_billingId;
            }

        } elseif (OrderStatus::isRefunded($result['orderStatus'])) {
            return UserProfile::findOne(Yii::$app->user->id)->bank_billingId;
        }

        return false;
    }

    public function payWithBind() {

        $orderModel = Order::findOne($this->id);

        $client = $this->initClientObject();

        $clientId = Yii::$app->user->id;
        $orderId = $orderModel->id;
        $orderAmount = $orderModel->amount_price*100;

        $params['clientId'] = $clientId;
        $params['features'] = 'AUTO_PAYMENT';

        $result = $client->registerOrder($orderId, $orderAmount, env('BASE_URL')  . env('SBER_SUCCESS_URL'), $params);

        if (isset($result['orderId']) && !empty($result['orderId'])) {
            $orderModel->bank_orderId = $result['orderId'];
            if ($orderModel->validate() && $orderModel->save()) {

                $orderId = $orderModel->bank_orderId;
                $bindingId = UserProfile::findOne(Yii::$app->user->id)->bank_billingId;
                $data['language'] = 'ru';

                $client->paymentOrderBinding($orderId, $bindingId, $data);

                $status = $client->getOrderStatus($orderId);

                if (OrderStatus::isDeposited($status['orderStatus'])) {
                    Order::setStatus($this->id, Order::LIVE_STATUS_PAID);
                    return array(
                        'id' => $orderModel->id
                    );
                }
            }
        }

        return array(
            'error' => "Произошла ощибка при оплате заказа!"
        );

    }

    private function initClientObject() {

        return new Client([
            'userName' => env('SBER_LOGIN'),
            'password' => env('SBER_PASS'),
            'language' => 'ru',
            'currency' => Currency::RUB,
            'apiUri' => Client::API_URI_TEST
        ]);

    }

}