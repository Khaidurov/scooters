<?php
namespace  common\modules\api\models\form;

use common\models\IssuePoint;
use common\models\Tariff;
use yii\base\Model;

/**
 * IssuePointDetail form
 */
class IssuePointDetailForm extends Model
{
    public $id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'trim'],
            [['id'], 'required'],
            [['id'], 'integer'],
            [['id'], 'exist', 'targetClass' => IssuePoint::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    public function getDetailIssuePoint()
    {
        $IssuePointModel = IssuePoint::findOne($this->id);

        $issue_point = IssuePoint::getIssuePointDetail($IssuePointModel->id);
        $tariffs = Tariff::getTariffsByDeviceType($IssuePointModel->device_type_id);

        if (!$issue_point || !$tariffs) {

            return array(
                'error' => 'Не удалось найти точку выдачи или тарифы для нее',
            );

        } else {

            return array(
                'issue_point' => $issue_point,
                'tariffs' => $tariffs,
            );

        }
    }

}
