<?php
namespace  common\modules\api\models\form;

use common\models\Device;
use common\models\Order;
use yii\base\Model;

/**
 * OrderCancel form
 */
class OrderCancelForm extends Model
{
    public $id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'trim'],
            [['id'], 'required'],
            [['id'], 'integer'],
            [['id'], 'exist', 'targetClass' => Order::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    public function cancel()
    {
        $model = Order::findOne($this->id);

        Order::setStatus($model->id, Order::LIVE_CANCELED);
        Device::afterEndOrder($model->device_id, $model->issue_point_start_id);

        return array(
            'id' => $model->id
        );
    }

}