<?php
namespace  common\modules\api\models\form;

use common\models\Order;
use yii\base\Model;

/**
 * OrderGetStatus form
 */
class OrderGetStatusForm extends Model
{
    public $id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'trim'],
            [['id'], 'required'],
            [['id'], 'integer'],
            [['id'], 'exist', 'targetClass' => Order::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

}
