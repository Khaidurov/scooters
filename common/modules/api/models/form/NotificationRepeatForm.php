<?php
namespace  common\modules\api\models\form;

use common\models\Notification;
use common\modules\user\models\User;
use Yii;
use yii\base\Model;

/**
 * NotificationRepeat form
 */
class NotificationRepeatForm extends Model
{
    public $id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'trim'],
            [['id'], 'required'],
            [['id'], 'integer'],
            [['id'], 'exist', 'targetClass' => User::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    public function sendCode()
    {
        if (!Notification::SendCode(User::findIdentity($this->id)->phone_number)) {
            return array(
                'error' => 'Нельзя отправлять код слишком часто'
            );
        }

        return array(
            'success' => true
        );
    }

}