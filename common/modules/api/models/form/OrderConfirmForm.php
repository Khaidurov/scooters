<?php
namespace  common\modules\api\models\form;

use common\models\Order;
use yii\base\Model;

/**
 * OrderConfirm form
 */
class OrderConfirmForm extends Model
{
    public $id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'trim'],
            [['id'], 'required'],
            [['id'], 'integer'],
            [['id'], 'exist', 'targetClass' => Order::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    public function confirm()
    {
        Order::setStatus($this->id, Order::LIVE_STATUS_LEASE);

        return array(
            'id' => $this->id
        );
    }

}