<?php
namespace  common\modules\api\models\form;

use common\models\Order;
use yii\base\Model;

/**
 * UserOrders form
 */
class OrderPriceForm extends Model
{
    public $order_id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id'], 'trim'],
            [['order_id'], 'required'],
            [['order_id'], 'integer'],
            [['order_id'], 'exist', 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    public function getPrice()
    {
        $price = Order::findOne($this->order_id)->amount_price;

        if (!$price) {

            return array(
                'error' => 'Не удалось получить стоимость заказа',
            );

        } else {

            return array(
                'price' => $price
            );

        }
    }

}
