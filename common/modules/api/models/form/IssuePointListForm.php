<?php
namespace  common\modules\api\models\form;

use yii\base\Model;

/**
 * IssuePointList form
 */
class IssuePointListForm extends Model
{
    public $lat;
    public $lng;
    public $r;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lat', 'lng', 'r'], 'trim'],
            [['lat', 'lng', 'r'], 'required'],
            [['lat', 'lng', 'r'], 'double'],
        ];
    }

}
