<?php
namespace  common\modules\api\models\form;

use common\models\Order;
use yii\base\Model;

/**
 * OrderDetail form
 */
class OrderDetailForm extends Model
{
    public $id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'trim'],
            [['id'], 'required'],
            [['id'], 'integer'],
            [['id'], 'exist', 'targetClass' => Order::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    public function getOrderDetail()
    {
        $order = Order::getOrderById($this->id);

        if (!$order) {

            return array(
                'error' => 'Не удалось найти заказ',
            );

        } else {

            return $order;

        }
    }

}
