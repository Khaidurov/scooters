<?php
namespace  common\modules\api\models\form;

use common\models\Tariff;
use yii\base\Model;

/**
 * TariffDetail form
 */
class TariffDetailForm extends Model
{
    public $id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'trim'],
            [['id'], 'required'],
            [['id'], 'integer'],
            [['id'], 'exist', 'targetClass' => Tariff::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    public function getTariffDetail()
    {
        $tariff = Tariff::getTariffById($this->id);

        if (!$tariff) {

            return array(
                'error' => 'Не удалось найти тариф',
            );

        } else {

            return $tariff;

        }
    }

}
