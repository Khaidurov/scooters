<?php
namespace  common\modules\api\models\form;

use common\modules\user\models\User;
use common\modules\userprofile\models\UserProfile;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * SetUserData form
 */
class SetUserDataForm extends Model
{
    public $email;
    public $name;
    public $surname;
    public $middle_name;
    public $passport_series;
    public $passport_number;
    public $passport_img_first;
    public $passport_img_second;
    public $passport_img_third;
    public $city;
    public $date_of_birth;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'middle_name', 'passport_series', 'passport_number'], 'required'],
            [['email', 'name', 'surname', 'middle_name', 'city', 'date_of_birth', 'passport_series', 'passport_number'], 'trim'],
            [['date_of_birth'], 'integer'],
            ['passport_series', 'integer', 'min' => 1000, 'max' => 9999],
            ['passport_number', 'integer', 'min' => 100000, 'max' => 999999],
            [['passport_img_first', 'passport_img_second', 'passport_img_third'], 'file', 'extensions' => 'jpeg, jpg, png'],
            ['email', 'email'],
            [['name', 'surname', 'middle_name', 'city'], 'string', 'max' => 255],

        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'ID',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
            'date_of_birth' => 'Дата рождения',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'middle_name' => 'Отчество',
            'city' => 'Город',
            'bank_card' => 'Банковская карта',
            'passport_img' => 'Паспорт',
            'signature_img' => 'Подпись',
            'birthDateFormat' => 'Дата рождения',
            'status' => 'Статус',
            'passport_series' => 'Серия паспорта',
            'passport_number' => 'Номер паспорта',
            'phone' => 'Тел.',
        ];
    }

    public function setUserData()
    {
        if (!User::setEmail(Yii::$app->user->id, $this->email) ||
            !UserProfile::setUserData(Yii::$app->user->id, $this->name, $this->surname, $this->middle_name, $this->city, $this->date_of_birth, $this->passport_series, $this->passport_number, UploadedFile::getInstance($this, 'passport_img_first'), UploadedFile::getInstance($this, 'passport_img_second'), UploadedFile::getInstance($this, 'passport_img_third'))) {

            return array(
                'error' => 'Ошибка заполенния пользовательских данных'
            );

        }

        UserProfile::setSatatus(UserProfile::STATUS_PERSONAL_DATA_FILED);

        return array(
            'id' => Yii::$app->user->id
        );

    }

}