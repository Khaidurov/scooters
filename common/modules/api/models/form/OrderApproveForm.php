<?php
namespace  common\modules\api\models\form;

use common\models\Device;
use common\models\IssuePoint;
use common\models\Order;
use yii\base\Model;

/**
 * OrderApprove form
 */
class OrderApproveForm extends Model
{
    public $id;
    public $issue_point_id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'issue_point_id'], 'trim'],
            [['id', 'issue_point_id'], 'required'],
            [['id', 'issue_point_id'], 'integer'],
            [['id'], 'exist', 'targetClass' => Order::className(), 'targetAttribute' => ['id' => 'id']],
            [['issue_point_id'], 'exist', 'targetClass' => IssuePoint::className(), 'targetAttribute' => ['issue_point_id' => 'id']],
        ];
    }

    public function approve()
    {
        $model = Order::findOne($this->id);
        $model->issue_point_end_id = $this->issue_point_id;
        $model->live_status = Order::LIVE_STATUS_END;

        if ($model->validate() && $model->save()) {

            return array(
                'id' => $model->id
            );

        } else {

            return array(
                'error' => "Ошибка подтверждения заказа"
            );

        }
    }

}