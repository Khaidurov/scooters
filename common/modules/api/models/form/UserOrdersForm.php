<?php
namespace  common\modules\api\models\form;

use common\models\Order;
use common\modules\user\models\User;
use yii\base\Model;

/**
 * UserOrders form
 */
class UserOrdersForm extends Model
{
    public function getUserOrders()
    {
        $orders = Order::getOrdersByUser(\Yii::$app->user->id);

        if (!$orders) {

            return array(
                'error' => 'У вас еще нет заказов',
            );

        } else {

            return $orders;

        }
    }

}
