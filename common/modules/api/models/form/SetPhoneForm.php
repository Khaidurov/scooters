<?php
namespace  common\modules\api\models\form;

use Yii;
use yii\base\Model;
use borales\extensions\phoneInput\PhoneInputValidator;
use common\modules\user\models\User;
use common\models\Notification;

/**
 * SetPhone form
 */
class SetPhoneForm extends Model
{
    public $tel;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tel'], 'trim'],
            [['tel'], 'required'],
            [['tel'], PhoneInputValidator::className(), 'region' => ['RU']]
        ];
    }

    public function beforeValidate()
    {
        if (!empty($this->tel)) {
            $this->tel = User::clearPhoneNumber($this->tel);

        }
        return parent::beforeValidate();
    }

    public function signUp () {

        if ($this->validate()) {

            $userModel = new User();
            $userModel->phone_number = $this->tel;
            $userModel->setPassword(Yii::$app->getSecurity()->generateRandomString(8));

            if ($userModel->validate() && $userModel->save()) {

                Notification::SendCode($userModel->phone_number);
                return array(
                    'id' => $userModel->id
                );

            }
        }
    }

    public function checkByPhoneNumber()
    {
        $this->tel = User::clearPhoneNumber($this->tel);
        return User::find()->where(['phone_number' => $this->tel])->exists();
    }

    public function getExistUser()
    {
        $model = User::findByPhoneNumber($this->tel);



        Notification::SendCode($model->phone_number);
        return array(
            'id' => $model->id
        );
    }

}
