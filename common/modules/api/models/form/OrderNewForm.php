<?php
namespace  common\modules\api\models\form;

use common\models\Tariff;
use Yii;
use yii\base\Model;
use common\models\Device;
use common\models\IssuePoint;
use common\models\Order;

/**
 * OrderNew form
 */
class OrderNewForm extends Model
{
    public $issue_point;
    public $tariff;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['issue_point', 'tariff'], 'trim'],
            [['issue_point', 'tariff'], 'required'],
            [['issue_point', 'tariff'], 'integer'],
            [['issue_point'], 'exist', 'targetClass' => IssuePoint::className(), 'targetAttribute' => ['issue_point' => 'id']],
            [['tariff'], 'exist', 'targetClass' => Tariff::className(), 'targetAttribute' => ['tariff' => 'id']],
        ];
    }

    public function createOrder()
    {

        $model = new Order();
        $issuePointModel = IssuePoint::findOne($this->issue_point);

        $model->issue_point_start_id = $issuePointModel->id;
        $model->tariff_id = $this->tariff;
        $model->device_id = Device::getDeviceMinUses($this->issue_point);
        $model->admin_start_id = $issuePointModel->admin_id;
        $model->live_status = $model::LIVE_STATUS_BOOKING;
        $model->client_id = Yii::$app->user->id;
        Device::setIssuePointAndStatus($model->device_id, null, Device::STATUS_BUSY);


        if ($model->validate() && $model->save()) {

            return array(
                'id' => $model->id
            );

        }
    }


}