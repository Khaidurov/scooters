<?php
namespace  common\modules\api\models\form;

use common\modules\user\models\User;
use Yii;
use yii\base\Model;

/**
 * SetAppToken form
 */
class SetAppTokenForm extends Model
{
    public $app_token;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['app_token'], 'trim'],
            [['app_token'], 'required'],
            [['app_token'], 'string', 'max' => 100],
        ];
    }

    public function setAppToken()
    {
        if (User::setAppToken($this->app_token)) {

            return array(
                'success' => true
            );

        } else {

            return array(
                'error' => "Не удалось задать токен приложения"
            );

        }
    }

}