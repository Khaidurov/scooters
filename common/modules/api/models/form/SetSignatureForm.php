<?php
namespace  common\modules\api\models\form;

use common\modules\userprofile\models\UserProfile;
use Yii;
use yii\base\Model;

/**
 * SetAppToken form
 */
class SetSignatureForm extends Model
{
    public $signature;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['signature'], 'trim'],
            [['signature'], 'required'],
            [['signature'], 'string'],
        ];
    }

    public function setSignature()
    {
        if (UserProfile::setSignature($this->signature)) {

            return array(
                'id' => Yii::$app->user->id
            );

        } else {

            return array(
                'error' => "Не удалось сохранить вашу подпись"
            );

        }
    }

}