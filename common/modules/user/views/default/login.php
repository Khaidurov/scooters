<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\modules\user\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Авторизация';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="fullpage-form-container">
    <div class="panel panel-primary panel-form">
        <div class="panel-heading" style="padding-top:2.5em; padding-bottom:2.5em">
            <h2 class="align-center bold" style="font-weight:400">Scooters</h2>
        </div>
        <!-- /.panel-cover -->

        <div class="panel-body">
            <h4 class="align-center form-heading"><?= $this->title?></h4>

            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'options' => [
                        'class' => 'mdform'
                ],
            ]); ?>

            <?= $form->field($model, 'login', ['options' => [
                    'class' => 'form-group md-form-group'
            ]])->textInput() ?>

            <?= $form->field($model, 'password', ['options' => [
                'class' => 'form-group md-form-group'
            ]])->passwordInput() ?>

            <?= $form->field($model, 'rememberMe', ['options' => [
                'class' => 'form-group checkbox'
            ]])->checkbox(['class' => 'icheck']) ?>

            <div class="form-group">
                <?= Html::submitButton('ВОЙТИ', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>


        </div>
        <!-- /.panel-body -->

    </div>
    <!-- /.panel-primary panel -->
</div>
