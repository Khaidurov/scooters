<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use backend\assets\AdminThemeAsset;

AppAsset::register($this);
AdminThemeAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style>
        #loader-container {
            display: block;
            position: fixed;
            z-index: 100;
            left: 0;
            top:0;
            width: 100%;
            height: 100%;
            width: 100vw;
            height: 100vh;
            background: rgb(241,246,251);
            overflow: hidden;
        }
        #loader-container .loader-inner {
            text-align: center;
            position: absolute;
            top:40%;
            left:0;
            width: 100%;
            top:40vh;
            left:0;
            width: 100vw;
            transform: scale(1);

        }
        #loader-container .loader-inner div {
            background-color: #7359EE;
        }
    </style>
</head>
<body>
<?php $this->beginBody() ?>

<div class="page-wrapper">

    <?= $this->render(
        'loader.php'
    ) ?>

    <?= $this->render(
        'content.php',
        ['content' => $content]
    ) ?>

</div>



<?php $this->endBody() ?>
<script>
    $(function(){
        $('.datatable').DataTable()
    });
</script>
</body>

</html>
<?php $this->endPage() ?>
