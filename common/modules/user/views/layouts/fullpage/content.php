<?php
/* @var $content string */

use common\widgets\Alert;

?>

<div class="page full-width">
    <div class="page-content">
        <div class="container-fluid">
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>
</div>