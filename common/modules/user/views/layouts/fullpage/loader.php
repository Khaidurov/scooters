<div id="loader-container" style="display:none">
    <div id="loader">
        <div class="loader-inner line-scale">
            <div></div><div></div><div></div><div></div><div></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var loadersign = document.getElementById('loader-container');
    loadersign.style.display = "block";
    window.onload = function() {
        loadersign.style.display = "none";
    }
</script>