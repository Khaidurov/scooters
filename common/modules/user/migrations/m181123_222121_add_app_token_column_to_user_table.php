<?php

use yii\db\Migration;

/**
 * Handles adding app_token to table `user`.
 */
class m181123_222121_add_app_token_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'app_token', $this->string(40));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'app_token');
    }
}
