<?php

use yii\db\Migration;

/**
 * Handles adding login to table `user`.
 */
class m180929_184213_add_login_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'login', $this->string(40));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'login');
    }
}
