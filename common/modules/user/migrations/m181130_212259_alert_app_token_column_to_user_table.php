<?php

use yii\db\Migration;

/**
 * Class m181130_212259_alert_app_token_column_to_user_table
 */
class m181130_212259_alert_app_token_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('user', 'app_token', $this->string(100));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('user', 'app_token', $this->string(40));
    }
}
