1. Add to your config file settings like this:

...

'modules' => [
        'user' => [
                    'class' => 'common\modules\user\User',
        ],
],

...

'components' => [
        'user' => [
            'loginUrl' => ['user/default/login'],
            'identityClass' => 'common\modules\user\models\User',
        ],
},

...

2. Run migrations:

yii migrate --migrationPath=@common/modules/user/migrations/