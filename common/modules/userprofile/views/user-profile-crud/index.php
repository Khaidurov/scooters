<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\userprofile\models\search\UserProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Профили пользователей';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-profile-index">


    <?php Pjax::begin(); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            'user_id',
                            [
                                'attribute' => 'phone',
                                'value' => function($model) {
                                    return $model->user->phone_number;
                                },
                            ],
                            'name',
                            'surname',
                            'middle_name',
                            [
                                'attribute' => 'date_of_birth',
                                'value' => function($model) {
                                    return Yii::$app->formatter->format($model->date_of_birth, 'datetime' );
                                },
                                'filter' => DateRangePicker::widget([
                                    'model'=>$searchModel,
                                    'attribute'=>'birthTimeRange',
                                    'convertFormat'=>true,
                                    'language' => 'ru',
                                    'pluginOptions'=>[
                                        'timePicker'=>true,
                                        'timePickerIncrement'=>30,
                                        'locale'=>[
                                            'format'=>'d.n.Y H:i'
                                        ]
                                    ]
                                ])
                            ],
                            'city',
                            'passport_series',
                            'passport_number',
                            [
                                'attribute' => 'status',
                                'filter' => Select2::widget([
                                    'model'=>$searchModel,
                                    'attribute'=>'status',
                                    'data' => common\modules\userprofile\models\UserProfile::getSatatusList(),
                                    'options' => ['placeholder' => 'Выбрать статус'],

                                ]),
                                'value' => function($model) {
                                    return $model::getSatatusList()[$model->status];
                                },
                            ],

                        ['class' => 'yii\grid\ActionColumn'],
                        ],
                        'options' => ['class' => 'table-responsive'],
                        ]); ?>
                </div>
            </div>
        </div>
    </div>
    <?php Pjax::end(); ?>
</div>
