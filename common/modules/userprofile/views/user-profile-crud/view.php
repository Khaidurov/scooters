<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\userprofile\models\UserProfile */

$this->title = $model->surname . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'User Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-profile-view">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <p>
                        <?= Html::a('Обновить', ['update', 'id' => $model->user_id], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('Удалить', ['delete', 'id' => $model->user_id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </p>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'user_id',
                                [
                                    'attribute' => 'phone',
                                    'value' => function($model) {
                                        return $model->user->phone_number;
                                    },
                                ],
                                'name',
                                'surname',
                                'middle_name',
                                [
                                    'attribute' => 'date_of_birth',
                                    'value' => function($model) {
                                        return Yii::$app->formatter->format($model->date_of_birth, 'datetime' );
                                    },
                                ],
                                'city',
                                'passport_series',
                                'passport_number',
                                [
                                    'attribute' => 'status',
                                    'value' => function($model) {
                                        return $model::getSatatusList()[$model->status];
                                    },
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="panel panel-default user-profile-images">
                <div class="panel-heading">
                    <h2>Изображения</h2>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="panel-body">
                            <h4>Подпись</h4>
                            <img src="data:image/jpg;base64,<?= $model->signature_img ?>" class="image-popup" alt="">

                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel-body">
                            <h4>Первое изображение паспорта</h4>
                            <img src="<?= $model->getImageFileUrl('passport_img_first')?>" class="image-popup" alt="">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel-body">
                            <h4>Второе изображение паспорта</h4>
                            <img src="<?= $model->getImageFileUrl('passport_img_second')?>" class="image-popup" alt="">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel-body">
                            <h4>Третье изображение паспорта</h4>
                            <img src="<?= $model->getImageFileUrl('passport_img_third')?>" class="image-popup" alt="">
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
