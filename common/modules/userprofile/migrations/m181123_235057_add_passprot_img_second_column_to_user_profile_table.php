<?php

use yii\db\Migration;

/**
 * Handles adding passprot_img_second to table `user_profile`.
 */
class m181123_235057_add_passprot_img_second_column_to_user_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_profile', 'passport_img_second', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user_profile', 'passport_img_second');
    }
}
