<?php

use yii\db\Migration;

/**
 * Class m190628_021919_alert_signature_img_column_to_user_profile_table
 */
class m190628_021919_alert_signature_img_column_to_user_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('user_profile', 'signature_img', $this->binary());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('user_profile', 'signature_img', $this->string());
    }

}
