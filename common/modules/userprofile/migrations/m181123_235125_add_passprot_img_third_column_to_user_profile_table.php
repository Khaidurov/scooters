<?php

use yii\db\Migration;

/**
 * Handles adding passprot_img_third to table `user_profile`.
 */
class m181123_235125_add_passprot_img_third_column_to_user_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_profile', 'passport_img_third', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user_profile', 'passport_img_third');
    }
}
