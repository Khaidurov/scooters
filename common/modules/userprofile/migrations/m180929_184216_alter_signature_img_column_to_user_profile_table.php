<?php

use yii\db\Migration;

/**
 * Handles adding signature_img to table `user_profile`.
 */
class m180929_184216_alter_signature_img_column_to_user_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('user_profile', 'signature_img', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('user_profile', 'signature_img', $this->binary());
    }
}
