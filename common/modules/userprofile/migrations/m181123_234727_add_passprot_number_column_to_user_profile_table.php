<?php

use yii\db\Migration;

/**
 * Handles adding passprot_number to table `user_profile`.
 */
class m181123_234727_add_passprot_number_column_to_user_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_profile', 'passport_number', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user_profile', 'passport_number');
    }
}
