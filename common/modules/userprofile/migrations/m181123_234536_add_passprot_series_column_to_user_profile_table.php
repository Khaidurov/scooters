<?php

use yii\db\Migration;

/**
 * Handles adding passprot_series to table `user_profile`.
 */
class m181123_234536_add_passprot_series_column_to_user_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_profile', 'passport_series', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user_profile', 'passport_series');
    }
}
