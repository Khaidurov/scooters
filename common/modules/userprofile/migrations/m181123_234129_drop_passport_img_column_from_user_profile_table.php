<?php

use yii\db\Migration;

/**
 * Handles dropping passport_img from table `user_profile`.
 */
class m181123_234129_drop_passport_img_column_from_user_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('user_profile', 'passport_img');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('user_profile', 'passport_img', $this->string());
    }
}
