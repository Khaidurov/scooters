<?php

use yii\db\Migration;

/**
 * Handles adding passprot_img_first to table `user_profile`.
 */
class m181123_235034_add_passprot_img_first_column_to_user_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_profile', 'passport_img_first', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user_profile', 'passport_img_first');
    }
}
