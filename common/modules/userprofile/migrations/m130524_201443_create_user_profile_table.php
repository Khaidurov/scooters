<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_profile`.
 */
class m130524_201443_create_user_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_profile', [
            'user_id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'date_of_birth' => $this->integer(),
            'name' => $this->string(),
            'surname' => $this->string(),
            'middle_name' => $this->string(),
            'city' => $this->string(),
            'bank_card' => $this->string(20)->unique(),
            'passport_img' => $this->string(),

        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user_profile');
    }
}
