<?php

use yii\db\Migration;

/**
 * Handles adding signature_img to table `user_profile`.
 */
class m180929_184214_add_status_column_to_user_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_profile', 'status', $this->tinyInteger()->notNull()->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user_profile', 'status');
    }
}
