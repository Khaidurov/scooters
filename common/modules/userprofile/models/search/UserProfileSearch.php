<?php

namespace common\modules\userprofile\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\userprofile\models\UserProfile;
use kartik\daterange\DateRangeBehavior;

/**
 * UserProfileSearch represents the model behind the search form of `common\modules\userprofile\models\UserProfile`.
 */
class UserProfileSearch extends UserProfile
{
    public $createTimeRange;
    public $createTimeStart;
    public $createTimeEnd;

    public $updateTimeRange;
    public $updateTimeStart;
    public $updateTimeEnd;

    public $birthTimeRange;
    public $birthTimeStart;
    public $birthTimeEnd;

    public $phone;

    public function behaviors()
    {
        return [
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'createTimeRange',
                'dateStartAttribute' => 'createTimeStart',
                'dateEndAttribute' => 'createTimeEnd',
            ],
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'updateTimeRange',
                'dateStartAttribute' => 'updateTimeStart',
                'dateEndAttribute' => 'updateTimeEnd',
            ],
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'birthTimeRange',
                'dateStartAttribute' => 'birthTimeStart',
                'dateEndAttribute' => 'birthTimeEnd',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'created_at', 'updated_at', 'date_of_birth', 'status'], 'integer'],
            [['name', 'surname', 'middle_name', 'city', 'bank_card', 'passport_series', 'passport_number', 'phone'], 'safe'],
            [['createTimeRange', 'updateTimeRange', 'birthTimeRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserProfile::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['user_id' => SORT_DESC]],
            'pagination' => [ 'pageSize' => 10 ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            $this::tableName() . '.' . 'user_id' => $this->user_id,
            $this::tableName() . '.' . 'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', $this::tableName() . '.' . 'name', $this->name])
            ->andFilterWhere(['like', $this::tableName() . '.' . 'surname', $this->surname])
            ->andFilterWhere(['like', $this::tableName() . '.' . 'middle_name', $this->middle_name])
            ->andFilterWhere(['like', $this::tableName() . '.' . 'city', $this->city])
            ->andFilterWhere(['like', $this::tableName() . '.' . 'bank_card', $this->bank_card])
            ->andFilterWhere(['like', $this::tableName() . '.' . 'passport_series', $this->passport_series])
            ->andFilterWhere(['like', $this::tableName() . '.' . 'passport_number', $this->passport_number]);

        $query->timestamp(
            $this::tableName(),
            $this->createTimeStart,
            $this->createTimeEnd,
            $this->updateTimeStart,
            $this->updateTimeEnd
        );

        $query->andFilterWhere(['>=',  $this::tableName() . '.' . 'date_of_birth', $this->birthTimeStart])
            ->andFilterWhere(['<',  $this::tableName() . '.' . 'date_of_birth', $this->birthTimeEnd]);

        $query->joinWith('user')->andFilterWhere(['like', 'user.phone_number', $this->phone]);

        return $dataProvider;
    }
}
