<?php

namespace common\modules\userprofile\models\query;

/**
 * This is the ActiveQuery class for [[UserProfile]].
 *
 * @see UserProfile
 */
class UserProfileQuery extends \common\models\query\SuperActiveQuery
{
    /**
     * {@inheritdoc}
     * @return UserProfile[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return UserProfile|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
