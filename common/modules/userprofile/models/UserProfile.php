<?php

namespace common\modules\userprofile\models;

use common\models\Device;
use common\models\DeviceType;
use common\models\IssuePoint;
use common\models\Order;
use common\models\Tariff;
use common\modules\article\models\Article;
use common\modules\article\models\ArticlesType;
use common\modules\faq\Faq;
use common\modules\user\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user_profile".
 *
 * @property int $user_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $date_of_birth
 * @property string $name
 * @property string $surname
 * @property string $middle_name
 * @property string $city
 * @property string $bank_card
 * @property string $passport_series
 * @property string $passport_number
 * @property string $passport_img_first
 * @property string $passport_img_second
 * @property string $passport_img_third
 * @property string $signature_img
 * @property string $bank_billingId
 * @property int $status
 */
class UserProfile extends \common\models\SuperAcriveRecord
{
    const STATUS_PHONE = 0;
    const STATUS_PHONE_OK = 1;
    const STATUS_CARD = 2;
    const STATUS_SIGNATURE = 3;
    const STATUS_PASSPORT = 4;
    const STATUS_CONFIRMED = 5;
    const STATUS_DEBTOR = 6;
    const STATUS_PERSONAL_DATA_FILED = 7;

    public $birthDateFormat;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_profile';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => '\yiidreamteam\upload\ImageUploadBehavior',
                'attribute' => 'passport_img_first',
                'filePath' => '@uploadImg/user_passport/[[pk]]_first.[[extension]]',
                'fileUrl' => '@publicImg/user_passport/[[pk]]_first.[[extension]]',
            ],
            [
                'class' => '\yiidreamteam\upload\ImageUploadBehavior',
                'attribute' => 'passport_img_second',
                'filePath' => '@uploadImg/user_passport/[[pk]]_second.[[extension]]',
                'fileUrl' => '@publicImg/user_passport/[[pk]]_second.[[extension]]',
            ],
            [
                'class' => '\yiidreamteam\upload\ImageUploadBehavior',
                'attribute' => 'passport_img_third',
                'filePath' => '@uploadImg/user_passport/[[pk]]_third.[[extension]]',
                'fileUrl' => '@publicImg/user_passport/[[pk]]_third.[[extension]]',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'date_of_birth', 'status'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_PHONE],
            [['name', 'surname', 'middle_name', 'city', 'bank_card', 'bank_billingId'], 'string', 'max' => 255],
            [['birthDateFormat'], 'string', 'max' => 20],
            [['signature_img'], 'string'],
            [['passport_img_first', 'passport_img_second', 'passport_img_third'], 'file', 'extensions' => 'jpeg, jpg, png'],
            [['bank_card'], 'unique'],
            ['passport_series', 'integer', 'max' => 9999],
            ['passport_number', 'integer', 'max' => 999999],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'ID',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
            'date_of_birth' => 'Дата рождения',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'middle_name' => 'Отчество',
            'city' => 'Город',
            'bank_card' => 'Банковская карта',
            'passport_img' => 'Паспорт',
            'signature_img' => 'Подпись',
            'birthDateFormat' => 'Дата рождения',
            'status' => 'Статус',
            'passport_series' => 'Серия паспорта',
            'passport_number' => 'Номер паспорта',
            'phone' => 'Тел.',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if ($this->birthDateFormat) {
                $this->date_of_birth = strtotime($this->birthDateFormat);
            }

            return true;
        }
        return false;
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            User::findOne($this->user_id)->delete();
            return true;
        }
        return false;
    }

    /**
     * @return array
     */
    public static function getSatatusList()
    {
        return array(
            self::STATUS_PHONE => 'Телефон задан',
            self::STATUS_PHONE_OK => 'Телефон подтвержден',
            self::STATUS_CARD => 'Карта добавлена',
            self::STATUS_SIGNATURE => 'Подпись добавлена',
            self::STATUS_PASSPORT => 'Пасспорт добавлен',
            self::STATUS_CONFIRMED => 'Подтвержден администратором',
            self::STATUS_DEBTOR => 'Должник',
            self::STATUS_PERSONAL_DATA_FILED => 'Добавлены персональные данные',
        );
    }

    /**
     * @return array
     */
    public static function getUserExistStatuses()
    {
        return array(
            self::STATUS_CARD,
            self::STATUS_SIGNATURE,
            self::STATUS_PASSPORT,
            self::STATUS_CONFIRMED,
            self::STATUS_DEBTOR,
            self::STATUS_PERSONAL_DATA_FILED,
        );
    }



    public static function findById($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    public static function setUserData($id, $name, $surname, $middle_name, $city, $date_of_birth, $passport_series, $passport_number, $passport_img_first, $passport_img_second, $passport_img_third)
    {
        $model = static::findOne($id);

        $model->name = $name;
        $model->surname = $surname;
        $model->middle_name = $middle_name;
        $model->city = $city;
        $model->date_of_birth = $date_of_birth;
        $model->passport_series = $passport_series;
        $model->passport_number = $passport_number;
        $model->passport_img_first = $passport_img_first;
        $model->passport_img_second = $passport_img_second;
        $model->passport_img_third= $passport_img_third;

        if ($model->save(false)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return int
     */
    public static function getSatatus()
    {
        return static::findOne(Yii::$app->user->id)->status;
    }

    /**
     * @return boolean
     */
    public static function setSatatus($status)
    {
        $model = static::findOne(Yii::$app->user->id);

        $model->status = $status;

        if ($model->validate() && $model->save()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return boolean
     */
    public static function setSatatusById($id, $status)
    {
        $model = static::findOne($id);

        $model->status = $status;

        if ($model->validate() && $model->save()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return boolean
     */
    public static function setBankCard($bank_card)
    {
        $model = static::findOne(Yii::$app->user->id);
        $model->bank_card = $bank_card;

        if ($model->validate() && $model->save()) {
            static::setSatatus(self::STATUS_CARD);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return boolean
     */
    public static function setSignature($signature)
    {
        $model = static::findOne(Yii::$app->user->id);
        $model->signature_img = $signature;

        if ($model->validate() && $model->save()) {
            static::setSatatus(self::STATUS_SIGNATURE);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return boolean
     */
    public static function setPassport($passport, $picture)
    {
        $model = static::findOne(Yii::$app->user->id);
        $model->$picture = $passport;

        if ($model->validate() && $model->save()) {
            $model->checkPassportImageFill();
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return boolean
     */
    public function checkPassportImageFill()
    {
        if ($this->passport_img_first != null && $this->passport_img_second != null && $this->passport_img_third != null)
        {
            static::setSatatus(self::STATUS_PASSPORT);
        }
    }

    /**
     * @return array
     */
    public static function getUserData()
    {
        return static::find()->select([
            'user_id',
            'name',
            'surname',
            'middle_name',
            'passport_series',
            'passport_number',
            'city',
            'date_of_birth'
        ])
        ->where(['user_id' => Yii::$app->user->id])
        ->asArray()
        ->one();
    }

    /**
     * @return array
     */
    public static function getIssuePointAdminList()
    {
        $ids = Yii::$app->authManager->getUserIdsByRole('issue_point_admin');

        $typeList = static::find()->select([
            'user_id',
            'user.login',
        ])
        ->joinWith('user user')
        ->where(['in', 'user_id', $ids])
        ->all();

        return ArrayHelper::map($typeList, 'user_id', 'user.login');
    }


    /**
     * ***********************************************************************************************
     * Relations section START
     * ***********************************************************************************************
     */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Yii::$app->user->identityClass, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFaqsCreatedBy()
    {
        return $this->hasMany(Faq::className(), ['created_by' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFaqsUpdatedBy()
    {
        return $this->hasMany(Faq::className(), ['updated_by' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticlesTypesCreatedBy()
    {
        return $this->hasMany(ArticlesType::className(), ['created_by' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticlesTypesUpdatedBy()
    {
        return $this->hasMany(ArticlesType::className(), ['updated_by' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticlesCreatedBy()
    {
        return $this->hasMany(Article::className(), ['created_by' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticlesUpdatedBy()
    {
        return $this->hasMany(Article::className(), ['updated_by' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeviceTypesCreatedBy()
    {
        return $this->hasMany(DeviceType::className(), ['created_by' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeviceTypesUpdatedBy()
    {
        return $this->hasMany(DeviceType::className(), ['updated_by' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffsCreatedBy()
    {
        return $this->hasMany(Tariff::className(), ['created_by' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffsUpdatedBy()
    {
        return $this->hasMany(Tariff::className(), ['updated_by' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIssuePointsCreatedBy()
    {
        return $this->hasMany(IssuePoint::className(), ['created_by' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIssuePointsUpdatedBy()
    {
        return $this->hasMany(IssuePoint::className(), ['updated_by' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIssuePointsAdmin()
    {
        return $this->hasMany(IssuePoint::className(), ['admin_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevicesCreatedBy()
    {
        return $this->hasMany(Device::className(), ['created_by' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevicesUpdatedBy()
    {
        return $this->hasMany(Device::className(), ['updated_by' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersCreatedBy()
    {
        return $this->hasMany(Order::className(), ['created_by' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersUpdatedBy()
    {
        return $this->hasMany(Order::className(), ['updated_by' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersAdminEnd()
    {
        return $this->hasMany(Order::className(), ['admin_end_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersAdminStart()
    {
        return $this->hasMany(Order::className(), ['admin_start_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersClient()
    {
        return $this->hasMany(Order::className(), ['client_id' => 'user_id']);
    }

    /**
     * ***********************************************************************************************
     * Relations section END
     * ***********************************************************************************************
     */

    /**
     * {@inheritdoc}
     * @return \common\modules\userprofile\models\query\UserProfileQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\userprofile\models\query\UserProfileQuery(get_called_class());
    }
}
