<?php

use yii\db\Migration;

/**
 * Handles the creation of table `articles_type`.
 */
class m180816_200047_create_articles_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('articles_type', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),

            'name' => $this->string(),

            'status' => $this->tinyInteger()->notNull()->defaultValue(1),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('articles_type');
    }
}
