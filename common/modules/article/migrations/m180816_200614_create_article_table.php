<?php

use yii\db\Migration;

/**
 * Handles the creation of table `article`.
 */
class m180816_200614_create_article_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('article', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),

            'title' => $this->string(),
            'desc' => $this->text(),
            'type_id' => $this->integer()->notNull(),

            'status' => $this->tinyInteger()->notNull()->defaultValue(1),
        ]);

        $this->addForeignKey(
            'article-articles_type',
            'article',
            'type_id',
            'articles_type',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('article');

        $this->dropForeignKey(
            'article-articles_type',
            'articles_type'
        );
    }
}
