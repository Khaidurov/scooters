1. Add to your config file settings like this:

...

'modules' => [
        'article' => [
                    'class' => 'common\modules\article\Article',
        ],
],

...


2. Run migrations:

yii migrate --migrationPath=@common/modules/article/migrations/