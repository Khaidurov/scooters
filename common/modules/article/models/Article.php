<?php

namespace common\modules\article\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "article".
 *
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property string $title
 * @property string $desc
 * @property int $type_id
 * @property int $status
 *
 * @property ArticlesType $type
 * @property Yii::$app->user->identityClass $createdBy
 * @property Yii::$app->user->identityClass $updatedBy
 */
class Article extends \common\models\SuperAcriveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'created_by', 'updated_by', 'type_id', 'status'], 'integer'],
            [['desc'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArticlesType::className(), 'targetAttribute' => ['type_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => Yii::$app->user->identityClass, 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => Yii::$app->user->identityClass, 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
            'created_by' => 'Создал',
            'updated_by' => 'Обновил',
            'title' => 'Заголовок',
            'desc' => 'Описание',
            'type_id' => 'Тип записи',
            'status' => 'Статус',
            'createdByEmail' => 'Создал',
            'updatedByEmail' => 'Обновил'
        ];
    }

    /**
     * @return array
     */
    public static function getTypeList()
    {
        $typeList = ArticlesType::find()->all();
        return ArrayHelper::map($typeList, 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(ArticlesType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Yii::$app->user->identityClass, ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(Yii::$app->user->identityClass, ['id' => 'updated_by']);
    }

    /**
     * {@inheritdoc}
     * @return \common\modules\article\models\query\ArticleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\article\models\query\ArticleQuery(get_called_class());
    }
}
