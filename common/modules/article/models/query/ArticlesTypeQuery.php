<?php

namespace common\modules\article\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\ArticlesType]].
 *
 * @see \common\models\ArticlesType
 */
class ArticlesTypeQuery extends \common\models\query\SuperActiveQuery
{
    /**
     * {@inheritdoc}
     * @return \common\models\ArticlesType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\ArticlesType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
