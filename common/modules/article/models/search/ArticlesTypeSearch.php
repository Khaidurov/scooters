<?php

namespace common\modules\article\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\article\models\ArticlesType;
use kartik\daterange\DateRangeBehavior;

/**
 * ArticlesTypeSearch represents the model behind the search form of `common\modules\article\models\ArticlesType`.
 */
class ArticlesTypeSearch extends ArticlesType
{
    public $createTimeRange;
    public $createTimeStart;
    public $createTimeEnd;

    public $updateTimeRange;
    public $updateTimeStart;
    public $updateTimeEnd;

    public $createdByEmail;
    public $updatedByEmail;

    public function behaviors()
    {
        return [
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'createTimeRange',
                'dateStartAttribute' => 'createTimeStart',
                'dateEndAttribute' => 'createTimeEnd',
            ],
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'updateTimeRange',
                'dateStartAttribute' => 'updateTimeStart',
                'dateEndAttribute' => 'updateTimeEnd',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['name','createdByEmail', 'updatedByEmail'], 'safe'],
            [['createTimeRange', 'updateTimeRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ArticlesType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => [ 'pageSize' => 10 ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            $this::tableName() . '.' . 'id' => $this->id,
            $this::tableName() . '.' . 'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', $this::tableName() . '.' . 'name', $this->name]);

        $query->timestamp(
            $this::tableName(),
            $this->createTimeStart,
            $this->createTimeEnd,
            $this->updateTimeStart,
            $this->updateTimeEnd
        );

        $query->blameable(
            $this->createdByEmail,
            $this->updatedByEmail
        );

        return $dataProvider;
    }
}
