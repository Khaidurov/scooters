<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\modules\article\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'desc')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'standard'
    ]) ?>

    <?= $form->field($model, 'type_id')->widget(Select2::classname(), [
        'data' => common\modules\article\models\Article::getTypeList(),
        'options' => ['placeholder' => 'Выбрать'],
    ]); ?>

    <?= $form->field($model, 'status')->widget(Select2::classname(), [
        'data' => common\modules\article\models\ArticlesType::getSatatusList(),
        'options' => ['placeholder' => 'Выбрать'],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
