<div class="row">
    <div class="col-lg-4">
        <a href="article/articles-type-crud/">
            <div class="panel">
                <div class="panel-body">
                    <div class="media">
                        <div class="media-left no-padding">
                            <div class="square-icon square-icon-primary square-icon-lg media-raised">
                                <i class="fa fa-list-alt"></i>
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="align-right" style="padding-top:0.3em;">
                                <h2>
                                    Articles Type
                                </h2>
                            </div>
                        </div>
                        <!-- /.media-body -->
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </a>
    </div>
    <div class="col-lg-4">
        <a href="article/article-crud/">
            <div class="panel">
                <div class="panel-body">
                    <div class="media">
                        <div class="media-left no-padding">
                            <div class="square-icon square-icon-danger square-icon-lg media-raised">
                                <i class="fa fa-file"></i>
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="align-right" style="padding-top:0.3em;">
                                <h2>
                                    Article
                                </h2>
                            </div>
                        </div>
                        <!-- /.media-body -->
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </a>
    </div>
</div>
