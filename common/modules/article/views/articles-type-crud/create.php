<?php

/* @var $this yii\web\View */
/* @var $model common\modules\article\models\ArticlesType */

$this->title = 'Создать тип записи';
$this->params['breadcrumbs'][] = ['label' => 'Articles Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articles-type-create">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $this->render('_form', [
                    'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
