<?php

/* @var $this yii\web\View */
/* @var $model common\modules\article\models\ArticlesType */

$this->title = 'Обновить тип записи: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Articles Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="articles-type-update">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $this->render('_form', [
                    'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
