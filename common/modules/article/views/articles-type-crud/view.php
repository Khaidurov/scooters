<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\article\models\ArticlesType */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Тип записи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articles-type-view">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <p>
                        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                        'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                        'method' => 'post',
                        ],
                        ]) ?>
                    </p>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'name',
                            [
                                'attribute' => 'status',
                                'value' => function($model) {
                                    return $model::getSatatusList()[$model->status];
                                },
                            ],
                        ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
