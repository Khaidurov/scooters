<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $searchModel common\modules\article\models\search\ArticlesTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Тип записи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articles-type-index">

    <?php Pjax::begin(); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <p>
                        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
                    </p>
                </div>
                <div class="panel-body">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            'id',
                            'name',
                            [
                                'attribute' => 'status',
                                'filter' => Select2::widget([
                                    'model'=>$searchModel,
                                    'attribute'=>'status',
                                    'data' => common\modules\article\models\ArticlesType::getSatatusList(),
                                    'options' => ['placeholder' => 'Выбрать'],

                                ]),
                                'value' => function($model) {
                                    return $model::getSatatusList()[$model->status];
                                },
                            ],
                            ['class' => 'yii\grid\ActionColumn'],
                            ],
                            'options' => ['class' => 'table-responsive'],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
    <?php Pjax::end(); ?>
</div>
