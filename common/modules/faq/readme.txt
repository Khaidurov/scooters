1. Add to your config file settings like this:

...

'modules' => [
        'faq' => [
                    'class' => 'common\modules\faq\Faq',
        ],
],

...


2. Run migrations:

yii migrate --migrationPath=@common/modules/faq/migrations/