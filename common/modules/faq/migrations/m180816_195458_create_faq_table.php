<?php

use yii\db\Migration;

/**
 * Handles the creation of table `faq`.
 */
class m180816_195458_create_faq_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('faq', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),

            'question' => $this->string(),
            'answer' => $this->text(),

            'status' => $this->tinyInteger()->notNull()->defaultValue(1),

        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('faq');
    }
}
